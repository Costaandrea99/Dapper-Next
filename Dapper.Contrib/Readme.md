# Dapper.Contrib 

Dapper.Contrib contains a number of helper methods for inserting, getting, updating and deleting records using directly the objects that reppresent the models.
Dapper.Contrib necessity Dapper.DBMS dependency because is based on its Attributes and its Sql Adapters.
For more information about the function of Dapper.DBMS click [Here](https://github.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.DBMS). ([GitLab](https://gitlab.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.DBMS))


The full list of extension methods in Dapper.Contrib right now are:

## Get - GetAsync

```csharp

T Get<T>(id);
Task<T> GetAsync<T>(id);

```

Returns a single entity by a single id from table "Ts". <br />
Id must be marked with [Key] attribute. (Check it in Dapper.DBMS [Here](https://github.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.DBMS). ([GitLab](https://gitlab.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.DBMS))) <br />
Entities created from interfaces are tracked / intercepted for changes and used by the Update() extension for optimal performance.

Example: et one specific entity based on id.

```csharp

// Execution of Get Method
var car = connection.Get<Car>(1);

// Execution of Get Async Method
var car = await connection.GetAsync<Car>(1);

```	


## GetAll - GetAllAsync

```csharp

IEnumerable<T> GetAll<T>();
Task<IEnumerable<T>> GetAllAsync<T>();

```

Returns a list of entites from table "Ts". <br />
Id must be marked with [Key] attribute. (Check it in Dapper.DBMS [Here](https://github.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.DBMS). ([GitLab](https://gitlab.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.DBMS))) <br />
Entities created from interfaces are tracked/intercepted for changes and used by the Update() extension for optimal performance. 

Example: get a list of all entities in the table.

```csharp

// Execution of Get All Method
var car = connection.GetAll<Car>();

// Execution of Get All Async Method
var car = await connection.GetAllAsync<Car>();

```	


## Insert - InsertAsync

```csharp

long Insert<T>
Task<int> InsertAsync<T>

```

Inserts an entity into table "Ts" and returns identity id or number of inserted rows if inserting a list. <br />

Example: Insert one entity or a list of entities.

```csharp

// Single Entity
Car singleEntity = new Car() { Name = "Volvo" };

// List of Entity
List<Car> cars = new List<Car>()
{
	new Car() { Name = "Volvo" },
	new Car() { Name = "Bmw" }
};

// Execution of Insert Single Entity
connection.Insert(singleEntity);

// Execution of Insert List of Entity
connection.Insert(cars);

// Execution of Insert Async Single Entity
await connection.InsertAsync(singleEntity);

// Execution of Insert Async List of Entity
await connection.InsertAsync(cars);

```


## Update - UpdateAsync

```csharp

bool Update<T>
Task<bool> UpdateAsync<T>

```

Updates entity in table "Ts", checks if the entity is modified if the entity is tracked by the Get() extension. <br />

Example: update one entity or a list of entities.

```csharp

// Single Entity
Car singleEntity = new Car() { Id = 1, Name = "Volvo" };

// List of Entity
List<Car> cars = new List<Car>()
{
	new Car() { Id = 1, Name = "Volvo" },
	new Car() { Id = 2, Name = "Bmw" }
};

// Execution of Update Single Entity
connection.Update(singleEntity);

// Execution of Update List of Entity
connection.Update(cars);

// Execution of Update Async Single Entity
await connection.UpdateAsync(singleEntity);

// Execution of Update Async List of Entity
await connection.UpdateAsync(cars);

```


## Delete - DeleteAsync

```csharp

bool Delete<T>
Task<bool> DeleteAsync<T>

```

Delete an entity by the specified `[Key]` property. <br />

```csharp

// Entity to Delete
Car singleEntity = new Car() { Id = 1 };

// List of Entity to Delete
List<Car> cars = new List<Car>()
{
	new Car() { Id = 1 },
	new Car() { Id = 2 }
};

// Execution of Delete Single Entity
connection.Delete(singleEntity);

// Execution of Delete List of Entity
connection.Delete(cars);

// Execution of Delete Async Single Entity
await connection.DeleteAsync(singleEntity);

// Execution of Delete Async List of Entity
await connection.DeleteAsync(cars);

```


## DeleteAll - DeleteAllAsync

```csharp

bool DeleteAll<T>
Task<bool> DeleteAllAsync<T>

```

Delete ALL entities in the table. <br />

```csharp


// Execution of DeleteAll
connection.DeleteAll<Car>();

// Execution of DeleteAllAsync
await connection.DeleteAllAsync<Car>();


```



### Limitations and caveats

## SQLite

`SQLiteConnection` exposes an `Update` event that clashes with the `Update`
extension provided by Dapper.Contrib. There are 2 ways to deal with this.

1. Call the `Update` method explicitly from `SqlMapperExtensions`

    ```Csharp
	
    SqlMapperExtensions.Update(_conn, new Employee { Id = 1, Name = "Mercedes" });
	
    ```
2. Make the method signature unique by passing a type parameter to `Update`

    ```Csharp
	
    connection.Update<Car>(new Car() { Id = 1, Name = "Maruti" });
	
    ```
