﻿using Dapper.DBMS.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// This static class contains the method that extend SqlAdapter.
    /// </summary>
    public static class SqlAdapterContrib
    {

        /// <summary>
        /// Inserts <paramref name="entityToInsert"/> into the database, returning the Id of the row created.
        /// </summary>
        /// <param name="sqlAdapter">The SqlAdapter object.</param>
        /// <param name="connection">The connection to use.</param>
        /// <param name="transaction">The transaction to use.</param>
        /// <param name="commandTimeout">The command timeout to use.</param>
        /// <param name="tableName">The table to insert into.</param>
        /// <param name="columnList">The columns to set with this insert.</param>
        /// <param name="parameterList">The parameters to set for this insert.</param>
        /// <param name="keyProperties">The key columns in this table.</param>
        /// <param name="entityToInsert">The entity to insert.</param>
        /// <returns>The Id of the row created.</returns>
        public static int Insert(this SqlAdapter sqlAdapter, IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfoExtended> keyProperties, object entityToInsert)
        {
            if (sqlAdapter is FbAdapter) return ((FbAdapter)sqlAdapter).Insert(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is MySqlAdapter) return ((MySqlAdapter)sqlAdapter).Insert(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is PostgresAdapter) return ((PostgresAdapter)sqlAdapter).Insert(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is SqlCeServerAdapter) return ((SqlCeServerAdapter)sqlAdapter).Insert(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is SQLiteAdapter) return ((SQLiteAdapter)sqlAdapter).Insert(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is SqlServerAdapter) return ((SqlServerAdapter)sqlAdapter).Insert(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);

            throw new Exception("Method not implemented for this database yet!");
        }

        /// <summary>
        /// Inserts <paramref name="entityToInsert"/> into the database, returning the Id of the row created.
        /// </summary>
        /// <param name="sqlAdapter">The SqlAdapter object.</param>
        /// <param name="connection">The connection to use.</param>
        /// <param name="transaction">The transaction to use.</param>
        /// <param name="commandTimeout">The command timeout to use.</param>
        /// <param name="tableName">The table to insert into.</param>
        /// <param name="columnList">The columns to set with this insert.</param>
        /// <param name="parameterList">The parameters to set for this insert.</param>
        /// <param name="keyProperties">The key columns in this table.</param>
        /// <param name="entityToInsert">The entity to insert.</param>
        /// <returns>The Id of the row created.</returns>
        public static Task<int> InsertAsync(this SqlAdapter sqlAdapter, IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfoExtended> keyProperties, object entityToInsert)
        {
            if (sqlAdapter is FbAdapter) return ((FbAdapter)sqlAdapter).InsertAsync(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is MySqlAdapter) return ((MySqlAdapter)sqlAdapter).InsertAsync(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is PostgresAdapter) return ((PostgresAdapter)sqlAdapter).InsertAsync(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is SqlCeServerAdapter) return ((SqlCeServerAdapter)sqlAdapter).InsertAsync(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is SQLiteAdapter) return ((SQLiteAdapter)sqlAdapter).InsertAsync(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);
            if (sqlAdapter is SqlServerAdapter) return ((SqlServerAdapter)sqlAdapter).InsertAsync(connection, transaction, commandTimeout, tableName, columnList, parameterList, keyProperties, entityToInsert);

            throw new Exception("Method not implemented for this database yet!");
        }

    }
}
