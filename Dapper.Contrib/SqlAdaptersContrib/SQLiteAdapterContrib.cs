﻿using Dapper.DBMS.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// This static class contains the method that extend SQLiteAdapter.
    /// </summary>
    public static class SQLiteAdapterContrib
    {

        /// <summary>
        /// Inserts <paramref name="entityToInsert"/> into the database, returning the Id of the row created.
        /// </summary>
        /// <param name="sQLiteAdapter">The SQLiteAdapter object.</param>
        /// <param name="connection">The connection to use.</param>
        /// <param name="transaction">The transaction to use.</param>
        /// <param name="commandTimeout">The command timeout to use.</param>
        /// <param name="tableName">The table to insert into.</param>
        /// <param name="columnList">The columns to set with this insert.</param>
        /// <param name="parameterList">The parameters to set for this insert.</param>
        /// <param name="keyProperties">The key columns in this table.</param>
        /// <param name="entityToInsert">The entity to insert.</param>
        /// <returns>The Id of the row created.</returns>
        public static int Insert(this SQLiteAdapter sQLiteAdapter, IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfoExtended> keyProperties, object entityToInsert)
        {
            var cmd = $"INSERT INTO {sQLiteAdapter.GetTableNameFormat(tableName)} ({columnList}) VALUES ({parameterList}); SELECT last_insert_rowid() id";
            var multi = connection.QueryMultiple(cmd, entityToInsert, transaction, commandTimeout);

            var id = (int)multi.Read().First().id;
            var propertyInfos = keyProperties as PropertyInfoExtended[] ?? keyProperties.ToArray();
            if (propertyInfos.Length == 0) return id;

            var idProperty = propertyInfos[0];
            idProperty.Property.SetValue(entityToInsert, Convert.ChangeType(id, idProperty.Property.PropertyType), null);

            return id;
        }

        /// <summary>
        /// Inserts <paramref name="entityToInsert"/> into the database, returning the Id of the row created.
        /// </summary>
        /// <param name="sQLiteAdapter">The SQLiteAdapter object.</param>
        /// <param name="connection">The connection to use.</param>
        /// <param name="transaction">The transaction to use.</param>
        /// <param name="commandTimeout">The command timeout to use.</param>
        /// <param name="tableName">The table to insert into.</param>
        /// <param name="columnList">The columns to set with this insert.</param>
        /// <param name="parameterList">The parameters to set for this insert.</param>
        /// <param name="keyProperties">The key columns in this table.</param>
        /// <param name="entityToInsert">The entity to insert.</param>
        /// <returns>The Id of the row created.</returns>
        public static async Task<int> InsertAsync(this SQLiteAdapter sQLiteAdapter, IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfoExtended> keyProperties, object entityToInsert)
        {
            var cmd = $"INSERT INTO {sQLiteAdapter.GetTableNameFormat(tableName)} ({columnList}) VALUES ({parameterList}); SELECT last_insert_rowid() id";
            var multi = await connection.QueryMultipleAsync(cmd, entityToInsert, transaction, commandTimeout).ConfigureAwait(false);

            var id = (int)multi.Read().First().id;
            var pi = keyProperties as PropertyInfoExtended[] ?? keyProperties.ToArray();
            if (pi.Length == 0) return id;

            var idp = pi[0];
            idp.Property.SetValue(entityToInsert, Convert.ChangeType(id, idp.Property.PropertyType), null);

            return id;
        }

    }
}
