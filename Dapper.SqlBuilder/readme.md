# Dapper.SqlBuilder

Dapper.SqlBuilder contains methods (like LINQ) that represent the SQL commands that allow you to always get the query translated into the correct SQL syntax for the database that you are using.<br />
Every methods can accept at max 3 Generic Type object!<br />
Every method return the object SqlBuilder modified. It's useful to do all in one row of code.

## Methods

### Select

Rappresent the "Select" part of SQL query. Pass Models and array of object that contains the property of the Models.

Example:

```

// ... Some Code ...

SqlBuilder sqlBuilder = new SqlBuilder()
    .Select<Pages, Menu>((Pages page, Menu menu) => new object[] { page.IDPage, page.PageName, menu.IDMenu });
   
// ... Some Code ...

```


### From

Rappresent the "From" part of SQL Query. Pass the type of the model.

Example:

```

// ... Some Code ...

SqlBuilder sqlBuilder = new SqlBuilder()
    .From(typeof(Pages));
   
// ... Some Code ...

```


### Inner Join - Left Join - Right Join

Rappresent the "Join" part of SQL Query, passing the type of the model to join and the expression of the condition.<br />
The condition must be simple, like "ON TABLE_A.ID = TABLE_B.FOREIGN_KEY".

Example:

```

// ... Some Code ...

SqlBuilder sqlBuilder = new SqlBuilder()
    .From(typeof(Pages))
	.InnerJoin<Pages, Menu>(typeof(Menu), (Pages page, Menu menu) => page.IDMenu == menu.IDMenu);

    // It's the same code for Left and Right Join, change only the name of method.
	
   
// ... Some Code ...

```


### Where

Rappresent the "Where" part of SQL Query. Pass the expression of the condition.

Example:

```

// ... Some Code ...

SqlBuilder sqlBuilder = new SqlBuilder()
    .From(typeof(Books))
	.Where<Books>((Books book) => book.NumberPage >= 100 || book.AuthorName == "Fabrizio");	
   
// ... Some Code ...

```


### Like - LikeStartWith - LikeBetweenWith - LikeEndWith

Rappresent the "Like" part of SQL Query, passing the property selector of the column and the string to be "liked".

Example:

```

// ... Some Code ...

string likeString = "Name of book";

SqlBuilder sqlBuilder = new SqlBuilder()
    .From(typeof(Books))
	.Like((Books book) => b.Name, likeString);
   
// ... Some Code ...

```


### OrderByAscending - OrderByDescending

Rappresent the "Order By" part of SQL Query. It works like the .Select() Method.

Example:

```

// ... Some Code ...

SqlBuilder sqlBuilder = new SqlBuilder()
    .From(typeof(Books))
    .OrderByAscending<Books>((Books book) => new object[] { book.NumberPage, book.Name });
   
// ... Some Code ...

```


### Limit 

Rappresent the "LIMIT" part of SQL Query.

Example:

```

// ... Some Code ...

// Get the first 10 rows
SqlBuilder sqlBuilder = new SqlBuilder()
    .From(typeof(Books))
	.Limit(10);
   
// ... Some Code ...

```


### Offset 

Rappresent the "OFFSET" part of SQL Query.

Example:

```

// ... Some Code ...

// Get the rows after the first 10 
SqlBuilder sqlBuilder = new SqlBuilder()
    .From(typeof(Books))
	.Offset(10);
   
// ... Some Code ...

```


### BindStringSql - BindStringAndExecuteQuery

Rappresent the "Binding" method that must be called at the end of our SqlBuilder object.<br />
BindStringSql return a string that contains the SQL query and an out DynamicParameter with the parameters of the query.<br />
BindStringAndExecuteQuery bind the query and run it. After run it auto-mapped object if it need to and return Ienumerable<T>.

Example:

```

// ... Some Code ...


// Return SQL String
string sqlString = new SqlBuilder()
    .From(typeof(Books))
	.BindStringSql(dbConnection, out DynamicParameter queryParameters);
	
	
// Return directly list of books.
List<Books> = new SqlBuilder()
    .From(typeof(Books))
	.BindStringAndExecuteQuery(connection);
	
// ... Some Code ...

```