﻿using Dapper.DBMS.Attributes;
using Dapper.DBMS.Cache;
using Dapper.DBMS.Extensions;
using Dapper.SqlBuilder.Binder;
using Dapper.SqlBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dapper.SqlBuilder.Mapper
{

    /// <summary>
    /// Static class that useful for auto-mapping of object
    /// </summary>
    public static class SqlBuilderMapper
    {

        /// <summary>
        /// This Method Auto Mapping the query that the user received
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlBuilderBinder"></param>
        /// <param name="dbConnection"></param>
        /// <param name="sqlQuery"></param>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        public static IEnumerable<T> AutoMappingBasedOnBinder<T>(SqlBuilderBinder sqlBuilderBinder, IDbConnection dbConnection, string sqlQuery, object queryParams)
        {
            ExecuteControlForAutoMapping(sqlBuilderBinder);

            // Get Mapping Method
            Type[] arrGenericTypeForMapping = GetGenericTypeArrayForMapping(sqlBuilderBinder);
            MethodInfo mappingMethod = GetStaticMethodWithOverloadAndGenericType(typeof(SqlBuilderMapper), "Mapping", arrGenericTypeForMapping.Length);
            MethodInfo mappingMethodWithGenericType = mappingMethod.MakeGenericMethod(arrGenericTypeForMapping);

            // Get Execute Query Method
            Type[] arrGenericTypeForQuery = GetGenericTypeArrayForQuery(sqlBuilderBinder);
            MethodInfo queryMethod = GetStaticMethodWithOverloadAndGenericType(typeof(SqlMapper), "Query", arrGenericTypeForQuery.Length);
            MethodInfo queryMethodWithGenericType = queryMethod.MakeGenericMethod(arrGenericTypeForQuery);

            string splitValues = GetSplitValue(sqlBuilderBinder.ListJoinConditions);

            // Invoke query Method
            return (IEnumerable<T>)queryMethodWithGenericType.Invoke(null, new object[]
            {
                dbConnection,
                sqlQuery,
                mappingMethodWithGenericType.Invoke(null, new object[] { sqlBuilderBinder.ListJoinConditions }),
                queryParams,
                null,
                true,
                splitValues,
                null,
                null
            });
        }




        /// <summary>
        /// This method create dynamically the function that will be execute for mapping objects
        /// </summary>
        /// <typeparam name="TFromOut">Generic Type of FROM part and will be returned</typeparam>
        /// <typeparam name="TJoinOne">Generic Type of first join</typeparam>
        /// <param name="listJoinsCondition">The list of the condition of the join</param>
        /// <returns>Function for mapping</returns>
        public static Func<TFromOut, TJoinOne, TFromOut> Mapping<TFromOut, TJoinOne>(List<JoinCondition> listJoinsCondition)
        {
            // Get Generic Parameters
            ParameterExpression expressionParamFromOut = Expression.Parameter(typeof(TFromOut), "expressionParamFromOut");
            ParameterExpression expressionParamJoinOne = Expression.Parameter(typeof(TJoinOne), "expressionParamJoinOne");
            List<ParameterExpression> listExpressionParameters = new List<ParameterExpression>() { expressionParamFromOut, expressionParamJoinOne };

            // Set List of Expression for the joins !
            List<Expression> listExpressionMethod = new List<Expression>();
            foreach (JoinCondition joinCondition in listJoinsCondition)
                listExpressionMethod.AddRange(AddExpressionsForJoin(joinCondition, listExpressionParameters));

            // Craete Return Statement
            LabelTarget labelReturn = Expression.Label(typeof(TFromOut));
            GotoExpression returnExpression = Expression.Return(labelReturn, expressionParamFromOut, typeof(TFromOut));
            LabelExpression returnLabelDefault = Expression.Label(labelReturn, Expression.Default(typeof(TFromOut)));

            // Adding Return Expression to List
            listExpressionMethod.Add(returnExpression);
            listExpressionMethod.Add(returnLabelDefault);

            // Create Block Expression
            BlockExpression blockExpression = Expression.Block(listExpressionMethod);

            // Compile Block Expression and return Func
            return Expression.Lambda<Func<TFromOut, TJoinOne, TFromOut>>(blockExpression, listExpressionParameters).Compile();
        }

        /// <summary>
        /// This method create dynamically the function that will be execute for mapping objects
        /// </summary>
        /// <typeparam name="TFromOut">Generic Type of FROM part and will be returned</typeparam>
        /// <typeparam name="TJoinOne">Generic Type of first join</typeparam>
        /// <typeparam name="TJoinTwo">Generic Type of second join</typeparam>
        /// <param name="listJoinsCondition">The list of the condition of the join</param>
        /// <returns>Function for mapping</returns>
        public static Func<TFromOut, TJoinOne, TJoinTwo, TFromOut> Mapping<TFromOut, TJoinOne, TJoinTwo>(List<JoinCondition> listJoinsCondition)
        {
            // Get Generic Parameters
            ParameterExpression expressionParamFromOut = Expression.Parameter(typeof(TFromOut), "expressionParamFromOut");
            ParameterExpression expressionParamJoinOne = Expression.Parameter(typeof(TJoinOne), "expressionParamJoinOne");
            ParameterExpression expressionParamJoinTwo = Expression.Parameter(typeof(TJoinTwo), "expressionParamJoinTwo");
            List<ParameterExpression> listExpressionParameters = new List<ParameterExpression>() { expressionParamFromOut, expressionParamJoinOne, expressionParamJoinTwo };

            // Set List of Expression for the joins !
            List<Expression> listExpressionMethod = new List<Expression>();
            foreach (JoinCondition joinCondition in listJoinsCondition)
                listExpressionMethod.AddRange(AddExpressionsForJoin(joinCondition, listExpressionParameters));

            // Craete Return Statement
            LabelTarget labelReturn = Expression.Label(typeof(TFromOut));
            GotoExpression returnExpression = Expression.Return(labelReturn, expressionParamFromOut, typeof(TFromOut));
            LabelExpression returnLabelDefault = Expression.Label(labelReturn, Expression.Default(typeof(TFromOut)));

            // Adding Return Expression to List
            listExpressionMethod.Add(returnExpression);
            listExpressionMethod.Add(returnLabelDefault);

            // Create Block Expression
            BlockExpression blockExpression = Expression.Block(listExpressionMethod);

            // Compile Block Expression and return Func
            return Expression.Lambda<Func<TFromOut, TJoinOne, TJoinTwo, TFromOut>>(blockExpression, listExpressionParameters).Compile();

        }

        /// <summary>
        /// This method create dynamically the function that will be execute for mapping objects
        /// </summary>
        /// <typeparam name="TFromOut">Generic Type of FROM part and will be returned</typeparam>
        /// <typeparam name="TJoinOne">Generic Type of first join</typeparam>
        /// <typeparam name="TJoinTwo">Generic Type of second join</typeparam>
        /// <typeparam name="TJoinThree">Generic Type of third join</typeparam>
        /// <param name="listJoinsCondition">The list of the condition of the join</param>
        /// <returns>Function for mapping</returns>
        public static Func<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TFromOut> Mapping<TFromOut, TJoinOne, TJoinTwo, TJoinThree>(List<JoinCondition> listJoinsCondition)
        {
            // Get Generic Parameters
            ParameterExpression expressionParamFromOut = Expression.Parameter(typeof(TFromOut), "expressionParamFromOut");
            ParameterExpression expressionParamJoinOne = Expression.Parameter(typeof(TJoinOne), "expressionParamJoinOne");
            ParameterExpression expressionParamJoinTwo = Expression.Parameter(typeof(TJoinTwo), "expressionParamJoinTwo");
            ParameterExpression expressionParamJoinThree = Expression.Parameter(typeof(TJoinThree), "expressionParamJoinThree");
            List<ParameterExpression> listExpressionParameters = new List<ParameterExpression>() { expressionParamFromOut, expressionParamJoinOne, expressionParamJoinTwo, expressionParamJoinThree };

            // Set List of Expression for the joins !
            List<Expression> listExpressionMethod = new List<Expression>();
            foreach (JoinCondition joinCondition in listJoinsCondition)
                listExpressionMethod.AddRange(AddExpressionsForJoin(joinCondition, listExpressionParameters));

            // Craete Return Statement
            LabelTarget labelReturn = Expression.Label(typeof(TFromOut));
            GotoExpression returnExpression = Expression.Return(labelReturn, expressionParamFromOut, typeof(TFromOut));
            LabelExpression returnLabelDefault = Expression.Label(labelReturn, Expression.Default(typeof(TFromOut)));

            // Adding Return Expression to List
            listExpressionMethod.Add(returnExpression);
            listExpressionMethod.Add(returnLabelDefault);

            // Create Block Expression
            BlockExpression blockExpression = Expression.Block(listExpressionMethod);

            // Compile Block Expression and return Func
            return Expression.Lambda<Func<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TFromOut>>(blockExpression, listExpressionParameters).Compile();
        }

        /// <summary>
        /// This method create dynamically the function that will be execute for mapping objects
        /// </summary>
        /// <typeparam name="TFromOut">Generic Type of FROM part and will be returned</typeparam>
        /// <typeparam name="TJoinOne">Generic Type of first join</typeparam>
        /// <typeparam name="TJoinTwo">Generic Type of second join</typeparam>
        /// <typeparam name="TJoinThree">Generic Type of third join</typeparam>
        /// <typeparam name="TJoinFour">Generic Type of fourth join</typeparam>
        /// <param name="listJoinsCondition">The list of the condition of the join</param>
        /// <returns>Function for mapping</returns>
        public static Func<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour, TFromOut> Mapping<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour>(List<JoinCondition> listJoinsCondition)
        {
            // Get Generic Parameters
            ParameterExpression expressionParamFromOut = Expression.Parameter(typeof(TFromOut), "expressionParamFromOut");
            ParameterExpression expressionParamJoinOne = Expression.Parameter(typeof(TJoinOne), "expressionParamJoinOne");
            ParameterExpression expressionParamJoinTwo = Expression.Parameter(typeof(TJoinTwo), "expressionParamJoinTwo");
            ParameterExpression expressionParamJoinThree = Expression.Parameter(typeof(TJoinThree), "expressionParamJoinThree");
            ParameterExpression expressionParamJoinFour = Expression.Parameter(typeof(TJoinFour), "expressionParamJoinFour");
            List<ParameterExpression> listExpressionParameters = new List<ParameterExpression>() { expressionParamFromOut, expressionParamJoinOne, expressionParamJoinTwo, expressionParamJoinThree, expressionParamJoinFour };

            // Set List of Expression for the joins !
            List<Expression> listExpressionMethod = new List<Expression>();
            foreach (JoinCondition joinCondition in listJoinsCondition)
                listExpressionMethod.AddRange(AddExpressionsForJoin(joinCondition, listExpressionParameters));

            // Craete Return Statement
            LabelTarget labelReturn = Expression.Label(typeof(TFromOut));
            GotoExpression returnExpression = Expression.Return(labelReturn, expressionParamFromOut, typeof(TFromOut));
            LabelExpression returnLabelDefault = Expression.Label(labelReturn, Expression.Default(typeof(TFromOut)));

            // Adding Return Expression to List
            listExpressionMethod.Add(returnExpression);
            listExpressionMethod.Add(returnLabelDefault);

            // Create Block Expression
            BlockExpression blockExpression = Expression.Block(listExpressionMethod);

            // Compile Block Expression and return Func
            return Expression.Lambda<Func<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour, TFromOut>>(blockExpression, listExpressionParameters).Compile();
        }

        /// <summary>
        /// This method create dynamically the function that will be execute for mapping objects
        /// </summary>
        /// <typeparam name="TFromOut">Generic Type of FROM part and will be returned</typeparam>
        /// <typeparam name="TJoinOne">Generic Type of first join</typeparam>
        /// <typeparam name="TJoinTwo">Generic Type of second join</typeparam>
        /// <typeparam name="TJoinThree">Generic Type of third join</typeparam>
        /// <typeparam name="TJoinFour">Generic Type of fourth join</typeparam>
        /// <typeparam name="TJoinFive">Generic Type of fifth join</typeparam>
        /// <param name="listJoinsCondition">The list of the condition of the join</param>
        /// <returns>Function for mapping</returns>
        public static Func<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour, TJoinFive, TFromOut> Mapping<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour, TJoinFive>(List<JoinCondition> listJoinsCondition)
        {
            // Get Generic Parameters
            ParameterExpression expressionParamFromOut = Expression.Parameter(typeof(TFromOut), "expressionParamFromOut");
            ParameterExpression expressionParamJoinOne = Expression.Parameter(typeof(TJoinOne), "expressionParamJoinOne");
            ParameterExpression expressionParamJoinTwo = Expression.Parameter(typeof(TJoinTwo), "expressionParamJoinTwo");
            ParameterExpression expressionParamJoinThree = Expression.Parameter(typeof(TJoinThree), "expressionParamJoinThree");
            ParameterExpression expressionParamJoinFour = Expression.Parameter(typeof(TJoinFour), "expressionParamJoinFour");
            ParameterExpression expressionParamJoinFive = Expression.Parameter(typeof(TJoinFive), "expressionParamJoinFive");
            List<ParameterExpression> listExpressionParameters = new List<ParameterExpression>() { expressionParamFromOut, expressionParamJoinOne, expressionParamJoinTwo, expressionParamJoinThree, expressionParamJoinFour, expressionParamJoinFive };

            // Set List of Expression for the joins !
            List<Expression> listExpressionMethod = new List<Expression>();
            foreach (JoinCondition joinCondition in listJoinsCondition)
                listExpressionMethod.AddRange(AddExpressionsForJoin(joinCondition, listExpressionParameters));

            // Craete Return Statement
            LabelTarget labelReturn = Expression.Label(typeof(TFromOut));
            GotoExpression returnExpression = Expression.Return(labelReturn, expressionParamFromOut, typeof(TFromOut));
            LabelExpression returnLabelDefault = Expression.Label(labelReturn, Expression.Default(typeof(TFromOut)));

            // Adding Return Expression to List
            listExpressionMethod.Add(returnExpression);
            listExpressionMethod.Add(returnLabelDefault);

            // Create Block Expression
            BlockExpression blockExpression = Expression.Block(listExpressionMethod);

            // Compile Block Expression and return Func
            return Expression.Lambda<Func<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour, TJoinFive, TFromOut>>(blockExpression, listExpressionParameters).Compile();
        }

        /// <summary>
        /// This method create dynamically the function that will be execute for mapping objects
        /// </summary>
        /// <typeparam name="TFromOut">Generic Type of FROM part and will be returned</typeparam>
        /// <typeparam name="TJoinOne">Generic Type of first join</typeparam>
        /// <typeparam name="TJoinTwo">Generic Type of second join</typeparam>
        /// <typeparam name="TJoinThree">Generic Type of third join</typeparam>
        /// <typeparam name="TJoinFour">Generic Type of fourth join</typeparam>
        /// <typeparam name="TJoinFive">Generic Type of fifth join</typeparam>
        /// <typeparam name="TJoinSix">Generic Type of sixth join</typeparam>
        /// <param name="listJoinsCondition">The list of the condition of the join</param>
        /// <returns>Function for mapping</returns>
        public static Func<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour, TJoinFive, TJoinSix, TFromOut> Mapping<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour, TJoinFive, TJoinSix>(List<JoinCondition> listJoinsCondition)
        {
            // Get Generic Parameters
            ParameterExpression expressionParamFromOut = Expression.Parameter(typeof(TFromOut), "expressionParamFromOut");
            ParameterExpression expressionParamJoinOne = Expression.Parameter(typeof(TJoinOne), "expressionParamJoinOne");
            ParameterExpression expressionParamJoinTwo = Expression.Parameter(typeof(TJoinTwo), "expressionParamJoinTwo");
            ParameterExpression expressionParamJoinThree = Expression.Parameter(typeof(TJoinThree), "expressionParamJoinThree");
            ParameterExpression expressionParamJoinFour = Expression.Parameter(typeof(TJoinFour), "expressionParamJoinFour");
            ParameterExpression expressionParamJoinFive = Expression.Parameter(typeof(TJoinFive), "expressionParamJoinFive");
            ParameterExpression expressionParamJoinSix = Expression.Parameter(typeof(TJoinSix), "expressionParamJoinSix");
            List<ParameterExpression> listExpressionParameters = new List<ParameterExpression>() { expressionParamFromOut, expressionParamJoinOne, expressionParamJoinTwo, expressionParamJoinThree, expressionParamJoinFour, expressionParamJoinFive, expressionParamJoinSix };

            // Set List of Expression for the joins !
            List<Expression> listExpressionMethod = new List<Expression>();
            foreach (JoinCondition joinCondition in listJoinsCondition)
                listExpressionMethod.AddRange(AddExpressionsForJoin(joinCondition, listExpressionParameters));

            // Craete Return Statement
            LabelTarget labelReturn = Expression.Label(typeof(TFromOut));
            GotoExpression returnExpression = Expression.Return(labelReturn, expressionParamFromOut, typeof(TFromOut));
            LabelExpression returnLabelDefault = Expression.Label(labelReturn, Expression.Default(typeof(TFromOut)));

            // Adding Return Expression to List
            listExpressionMethod.Add(returnExpression);
            listExpressionMethod.Add(returnLabelDefault);


            // Create Block Expression
            BlockExpression blockExpression = Expression.Block(listExpressionMethod);

            // Compile Block Expression and return Func
            return Expression.Lambda<Func<TFromOut, TJoinOne, TJoinTwo, TJoinThree, TJoinFour, TJoinFive, TJoinSix, TFromOut>>(blockExpression, listExpressionParameters).Compile();
        }


        /********************
         * 
         *  Private Methods
         * 
         ********************/

        private static void ExecuteControlForAutoMapping(SqlBuilderBinder sqlBuilderBinder)
        {
            // Control Max 6 Joins
            if (sqlBuilderBinder.ListJoinConditions.Count > 6) throw new Exception("Auto Mapping of SqlBuilder work only with max 6 joins!");

            // The join can't be Right Join
            if (sqlBuilderBinder.ListJoinConditions.Find(f => f.TypeJoin == TypeJoin.RightJoin) != null) throw new Exception("Auto mapping of SqlBuilder not supporte right join yet.");

            // Control if condition is simple!
            foreach (JoinCondition joinCondition in sqlBuilderBinder.ListJoinConditions)
                if (joinCondition.ConditionToJoin.Left.Type != TypeNodeCondition.Property || joinCondition.ConditionToJoin.Right.Type != TypeNodeCondition.Property)
                    throw new Exception("The condion of the joins must be simple and between columns! (Es. A.COLUMN = B.COLUMN)");
        }
        private static List<Expression> AddExpressionsForJoin(JoinCondition joinCondition, List<ParameterExpression> listParametersExpression)
        {
            // I work like this: Inner / Left join joined B element on A.FOREIGN_KEY = B.KEY
            // The assignment is like A.OBJ_FOREIGN_KEY = B where B is my OBJ and is the Model Joined
            // But i don't know the position of the property in the condition that I received, well i search it
            PropertyInfoExtended propLeftOperandInExpression = GetPropLeftOperandForExpression(joinCondition);
            PropertyInfoExtended propObjectToMapped = GetPropObjectToMapped(propLeftOperandInExpression);
            if (propObjectToMapped != null)
            {
                // Check if is list (One to Many) or simple object (One to One)
                if (!IsListOrArray(propObjectToMapped))
                    return MapOneToOne(joinCondition, listParametersExpression, propLeftOperandInExpression, propObjectToMapped);
                else
                    throw new Exception("Mapping one to many not implemented yet!");
            }
            else
                throw new Exception("Object to be mapped not finded!");
        }
        private static PropertyInfoExtended GetPropLeftOperandForExpression(JoinCondition joinCondition)
        {
            if (joinCondition.TypeModelToJoin == joinCondition.ConditionToJoin.Left.Property.Property.DeclaringType)
                return joinCondition.ConditionToJoin.Right.Property;
            else
                return joinCondition.ConditionToJoin.Left.Property;
        }
        private static PropertyInfoExtended GetPropObjectToMapped(PropertyInfoExtended propLeftOperandInExpression)
        {
            List<PropertyInfoExtended> listPropertiesForeignKeyMappingObject = StaticCache.ForeignKeyMappingObjectPropertiesCache(propLeftOperandInExpression.Property.DeclaringType);
            foreach (PropertyInfoExtended prop in listPropertiesForeignKeyMappingObject)
            {
                var attrsForeignKeyMappingObject = prop.Property.GetCustomAttributes(typeof(ForeignKeyMappingObjectAttribute), true).ToList();
                Attribute findObjByAttr = (Attribute)attrsForeignKeyMappingObject.Find(f => ((ForeignKeyMappingObjectAttribute)f).PropertyName == propLeftOperandInExpression.Property.Name);
                if (findObjByAttr != null) return prop;
            }

            return null;
        }
        private static bool IsListOrArray(PropertyInfoExtended propertyExtended)
        {
            return typeof(IEnumerable<>).IsAssignableFrom(propertyExtended.Property.PropertyType);
        }
        private static List<Expression> MapOneToOne(JoinCondition joinCondition, List<ParameterExpression> listParametersExpression, PropertyInfoExtended propLeftOperandInExpression, PropertyInfoExtended propObjectToMapped)
        {
            ParameterExpression leftParamInExpression = GetParamByType(listParametersExpression, propLeftOperandInExpression.Property.DeclaringType);
            ParameterExpression rightParamInExpression = GetParamByType(listParametersExpression, joinCondition.TypeModelToJoin);

            if (leftParamInExpression != null && rightParamInExpression != null)
            {
                MemberExpression leftPropertyToAssign = Expression.PropertyOrField(leftParamInExpression, propObjectToMapped.Property.Name);
                return new List<Expression>()
                    {
                        Expression.Assign(leftPropertyToAssign, rightParamInExpression),
                    };
            }
            else
                throw new Exception("Not all two param expresion finded!");
        }
        private static ParameterExpression GetParamByType(List<ParameterExpression> listParametersExpression, Type typeToFind)
        {
            return listParametersExpression.Find(f => f.Type == typeToFind);
        }
        private static Type[] GetGenericTypeArrayForMapping(SqlBuilderBinder sqlBuilderBinder)
        {
            List<Type> types = new List<Type>();
            types.Add(sqlBuilderBinder.From);
            types.AddRange(sqlBuilderBinder.ListJoinConditions.Select(s => s.TypeModelToJoin));
            return types.ToArray();
        }
        private static Type[] GetGenericTypeArrayForQuery(SqlBuilderBinder sqlBuilderBinder)
        {
            List<Type> types = new List<Type>();
            types.Add(sqlBuilderBinder.From);
            types.AddRange(sqlBuilderBinder.ListJoinConditions.Select(s => s.TypeModelToJoin));
            types.Add(sqlBuilderBinder.From);
            return types.ToArray();
        }
        private static MethodInfo GetStaticMethodWithOverloadAndGenericType(Type typeStaticClass, string methodName, int numberGenericType)
        {
            MethodInfo staticMethod = typeStaticClass.GetMethods().ToList().Find(f => f.IsGenericMethodDefinition && f.GetGenericArguments().Length == numberGenericType && f.Name == methodName);
            if (staticMethod == null) throw new Exception("Static Method not finded!");
            return staticMethod;
        }
        private static string GetSplitValue(List<JoinCondition> joinConditions)
        {
            List<string> listNameColumnPrimaryKey = new List<string>();
            foreach(JoinCondition joinCondition in joinConditions)
            {
                List<PropertyInfoExtended> listPrimaryKey = StaticCache.KeyPropertiesCache(joinCondition.TypeModelToJoin);
                if (listPrimaryKey.Count != 0) listNameColumnPrimaryKey.Add(listPrimaryKey[0].ColumnName);
            }

            return string.Join(",", listNameColumnPrimaryKey);
        }

    }
}
