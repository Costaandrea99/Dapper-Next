﻿using Dapper.SqlBuilder.Binder;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// This static class contains the method that extend Sql Lite.
    /// </summary>
    public static class SQLiteAdapterSqlBuilder
    {

        /// <summary>
        /// Binding in the correct way the SQL query for Sql Lite Database.
        /// </summary>
        /// <param name="sQLiteAdapter">The SQLiteAdapter object.</param>
        /// <param name="sqlBuilderBinder">SqlBuilderBinder object</param>
        /// <param name="queryParams">out list of object that contains params of Query</param>
        /// <returns>StringBuilder that contains the SQL query.</returns>
        public static StringBuilder SqlBuilderBindQuery(this SQLiteAdapter sQLiteAdapter, SqlBuilderBinder sqlBuilderBinder, out DynamicParameters queryParams)
        {
            throw new Exception("Method not implemented for SQL Lite Database!");
        }

    }
}
