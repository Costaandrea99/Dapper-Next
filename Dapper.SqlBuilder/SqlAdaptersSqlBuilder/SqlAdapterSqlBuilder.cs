﻿using Dapper.SqlBuilder.Binder;
using System;
using System.Text;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// This static class contains the method that extend SqlAdapter.
    /// </summary>
    public static class SqlAdapterSqlBuilder
    {

        /// <summary>
        /// Binding in the correct way for this database the SQL query.
        /// </summary>
        /// <param name="sqlAdapter">The SqlAdapter object.</param>
        /// <param name="sqlBuilderBinder">SqlBuilderBinder object</param>
        /// <param name="queryParams">out list of object that contains params of Query</param>
        /// <returns>StringBuilder that contains the SQL query.</returns>
        public static StringBuilder SqlBuilderBindQuery(this SqlAdapter sqlAdapter, SqlBuilderBinder sqlBuilderBinder, out DynamicParameters queryParams)
        {
            if (sqlAdapter is FbAdapter) return ((FbAdapter)sqlAdapter).SqlBuilderBindQuery(sqlBuilderBinder, out queryParams);
            if (sqlAdapter is MySqlAdapter) return ((MySqlAdapter)sqlAdapter).SqlBuilderBindQuery(sqlBuilderBinder, out queryParams);
            if (sqlAdapter is PostgresAdapter) return ((PostgresAdapter)sqlAdapter).SqlBuilderBindQuery(sqlBuilderBinder, out queryParams);
            if (sqlAdapter is SqlCeServerAdapter) return ((SqlCeServerAdapter)sqlAdapter).SqlBuilderBindQuery(sqlBuilderBinder, out queryParams);
            if (sqlAdapter is SQLiteAdapter) return ((SQLiteAdapter)sqlAdapter).SqlBuilderBindQuery(sqlBuilderBinder, out queryParams);
            if (sqlAdapter is SqlServerAdapter) return ((SqlServerAdapter)sqlAdapter).SqlBuilderBindQuery(sqlBuilderBinder, out queryParams);

            throw new Exception("Method not implemented for this database yet!");
        }
        
    }
}
