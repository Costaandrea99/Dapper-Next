﻿using Dapper.SqlBuilder.Binder;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// This static class contains the method that extend FbAdapter.
    /// </summary>
    public static class FbAdapterSqlBuilder
    {

        /// <summary>
        /// Binding in the correct way the SQL query for FireBase Database.
        /// </summary>
        /// <param name="fbAdapter">The FbAdapter object.</param>
        /// <param name="sqlBuilderBinder">SqlBuilderBinder object</param>
        /// <param name="queryParams">out list of object that contains params of Query</param>
        /// <returns>StringBuilder that contains the SQL query.</returns>
        public static StringBuilder SqlBuilderBindQuery(this FbAdapter fbAdapter, SqlBuilderBinder sqlBuilderBinder, out DynamicParameters queryParams)
        {
            throw new Exception("Method not implemented for FireBase Database!");
        }

    }
}
