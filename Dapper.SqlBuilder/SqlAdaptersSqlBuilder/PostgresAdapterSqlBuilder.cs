﻿using Dapper.DBMS.Attributes;
using Dapper.DBMS.Cache;
using Dapper.DBMS.Extensions;
using Dapper.SqlBuilder.Binder;
using Dapper.SqlBuilder.Objects;
using Dapper.SqlBuilder.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// This static class contains the method that extend Postgres.
    /// </summary>
    public static class PostgresAdapterSqlBuilder
    {

        /// <summary>
        /// Binding in the correct way the SQL query for Postgres Database.
        /// </summary>
        /// <param name="postgresAdapter">The PostgresAdapter object.</param>
        /// <param name="sqlBuilderBinder">SqlBuilderBinder object</param>
        /// <param name="queryParams">out list of object that contains params of Query</param>
        /// <returns>StringBuilder that contains the SQL query.</returns>
        public static StringBuilder SqlBuilderBindQuery(this PostgresAdapter postgresAdapter, SqlBuilderBinder sqlBuilderBinder, out DynamicParameters queryParams)
        {
            int nListSelect = sqlBuilderBinder.ListSelect.Count,
                nListJoinCondition = sqlBuilderBinder.ListJoinConditions.Count,
                nListOrderBy = sqlBuilderBinder.ListOrderBy.Count;

            queryParams = new DynamicParameters();
            StringBuilder sbSqlQuery = new StringBuilder();
            Dictionary<Type, string> dictionaryModelAlias = Utils.GetDictionaryModelAlias(sqlBuilderBinder);

            // Binding "SELECT" 
            sbSqlQuery.Append("SELECT " + (sqlBuilderBinder.Distinct ? "DISTINCT" : "") + " " + (nListSelect == 0 ? "*" : string.Join(", ", sqlBuilderBinder.ListSelect.Select(s => BindSelectProperty(postgresAdapter, s, dictionaryModelAlias)).ToList())));

            // Binding "FROM"
            sbSqlQuery.Append(" FROM " + postgresAdapter.GetTableNameFormat(StaticCache.GetTableName(sqlBuilderBinder.From)) + " AS " + dictionaryModelAlias.GetModelAliasNameFromDictionary(sqlBuilderBinder.From));

            // Binding "JOIN"
            if (nListJoinCondition != 0)
                sbSqlQuery.Append(string.Join(" ", sqlBuilderBinder.ListJoinConditions.Select(s => BindJoinProperty(postgresAdapter, s, dictionaryModelAlias)).ToList()));

            // Binding "WHERE"
            if (sqlBuilderBinder.WhereCondition != null)
                sbSqlQuery.Append(" WHERE " + BindCondition(postgresAdapter, sqlBuilderBinder.WhereCondition, dictionaryModelAlias, queryParams, out queryParams));

            // Binding "ORDER BY"
            if (nListOrderBy != 0)
                sbSqlQuery.Append(" ORDER BY " + string.Join(", ", sqlBuilderBinder.ListOrderBy.Select(s => BindOrderBy(postgresAdapter, s, dictionaryModelAlias))));

            // Binding "LIMIT"
            if (sqlBuilderBinder.Limit != null && sqlBuilderBinder.Limit > 0)
                sbSqlQuery.Append(" LIMIT " + sqlBuilderBinder.Limit);

            // Binding "OFFSET"
            if (sqlBuilderBinder.Offset != null && sqlBuilderBinder.Offset >= 0)
                sbSqlQuery.Append(" OFFSET " + sqlBuilderBinder.Offset);

            return sbSqlQuery;
        }



        /*****************************************
         * 
         *  Private Method For Binding SQL Query
         * 
         *****************************************/

        private static string BindSelectProperty(PostgresAdapter postgresAdapter, PropertyInfoExtended propertyInfoExtended, Dictionary<Type, string> dictionaryModelAlias)
        {
            // Check if Property have Computed Attribute
            string propertyStringFormat = dictionaryModelAlias.GetModelAliasNameFromDictionary(propertyInfoExtended.Property.DeclaringType) + "." + postgresAdapter.GetColumnNameFormat(propertyInfoExtended.ColumnName);
            var findComputedAttribute = propertyInfoExtended.Property
                #if NETSTANDARD1_3
                    .GetType()
                #endif
                .GetCustomAttributes(true).FirstOrDefault(f => f is ComputedAttribute);
            if (findComputedAttribute != null)
            {
                switch (((ComputedAttribute)findComputedAttribute).TypeComputed)
                {
                    case TypeComputed.AVG: return "AVG(" + propertyStringFormat + ")";
                    case TypeComputed.COUNT: return "COUNT(" + propertyStringFormat + ")";
                    case TypeComputed.MAX: return "MAX(" + propertyStringFormat + ")";
                    case TypeComputed.MIN: return "MIN(" + propertyStringFormat + ")";
                    case TypeComputed.SUM: return "SUM(" + propertyStringFormat + ")";
                }
            }

            return propertyStringFormat;
        }
        private static string BindJoinProperty(PostgresAdapter postgresAdapter, JoinCondition joinCondition, Dictionary<Type, string> dictionaryModelAlias)
        {
            string condition = BindCondition(postgresAdapter, joinCondition.ConditionToJoin, dictionaryModelAlias, null, out DynamicParameters queryParams);

            switch (joinCondition.TypeJoin)
            {
                case TypeJoin.InnerJoin: return "INNER JOIN " + postgresAdapter.GetTableNameFormat(StaticCache.GetTableName(joinCondition.TypeModelToJoin)) + " AS " + dictionaryModelAlias.GetModelAliasNameFromDictionary(joinCondition.TypeModelToJoin) + " ON " + condition;
                case TypeJoin.LeftJoin: return "LEFT JOIN " + postgresAdapter.GetTableNameFormat(StaticCache.GetTableName(joinCondition.TypeModelToJoin)) + " AS " + dictionaryModelAlias.GetModelAliasNameFromDictionary(joinCondition.TypeModelToJoin) + " ON " + condition;
                case TypeJoin.RightJoin: return "RIGHT JOIN " + postgresAdapter.GetTableNameFormat(StaticCache.GetTableName(joinCondition.TypeModelToJoin)) + " AS " + dictionaryModelAlias.GetModelAliasNameFromDictionary(joinCondition.TypeModelToJoin) + " ON " + condition;
            }

            throw new Exception("Type Join NOT supported!");
        }
        private static string BindCondition(PostgresAdapter postgresAdapter, Condition condition, Dictionary<Type, string> dictionaryModelAlias, DynamicParameters queryParamsIn, out DynamicParameters queryParamsOut)
        {
            return "(" + BindNodeCondition(postgresAdapter, condition.Left, condition.Right, dictionaryModelAlias, queryParamsIn, out queryParamsOut) + " " + BindOperandCondition(condition.Operand) + " " + BindNodeCondition(postgresAdapter, condition.Right, condition.Left, dictionaryModelAlias, queryParamsIn, out queryParamsOut) + ")";
        }
        private static string BindNodeCondition(PostgresAdapter postgresAdapter, NodeCondition nodeCondition, NodeCondition oppositeNodeCondition, Dictionary<Type, string> dictionaryModelAlias, DynamicParameters queryparamsIn, out DynamicParameters queryParamsOut)
        {
            queryParamsOut = queryparamsIn;
            switch (nodeCondition.Type)
            {
                case TypeNodeCondition.Constant:
                    return "@" + queryParamsOut.AddDynamicParam(nodeCondition.Value, out queryParamsOut);
                case TypeNodeCondition.Property:
                    return dictionaryModelAlias.GetModelAliasNameFromDictionary(nodeCondition.Property.Property.DeclaringType) + "." + postgresAdapter.GetColumnNameFormat(nodeCondition.Property.ColumnName);
                case TypeNodeCondition.Condition:
                    return BindCondition(postgresAdapter, nodeCondition.Condition, dictionaryModelAlias, queryParamsOut, out queryParamsOut);
            }

            throw new Exception("Type of node condition NOT supported!");
        }
        private static string BindOperandCondition(TypeOperandCondition typeOperandCondition)
        {
            switch (typeOperandCondition)
            {
                case TypeOperandCondition.And: return "AND";
                case TypeOperandCondition.Equal: return "=";
                case TypeOperandCondition.GreaterThan: return ">";
                case TypeOperandCondition.GreaterThanOrEqual: return ">=";
                case TypeOperandCondition.LessThan: return "<";
                case TypeOperandCondition.LessThanOrEqual: return "<=";
                case TypeOperandCondition.Like: return "LIKE";
                case TypeOperandCondition.LikeBetweenWith: return "LIKE";
                case TypeOperandCondition.LikeEndWith: return "LIKE";
                case TypeOperandCondition.LikeStartWith: return "LIKE";
                case TypeOperandCondition.NotEqual: return "<>";
                case TypeOperandCondition.Or: return "OR";
            }

            throw new Exception("Type Operand Condition NOT supported!");
        }
        private static string BindOrderBy(PostgresAdapter postgresAdapter, OrderByInfo orderByInfo, Dictionary<Type, string> dictionaryModelAlias)
        {
            string propertyStringFormat = dictionaryModelAlias.GetModelAliasNameFromDictionary(orderByInfo.Property.Property.DeclaringType) + "." + postgresAdapter.GetTableNameFormat(orderByInfo.Property.ColumnName);

            switch (orderByInfo.TypeOrderBy)
            {
                case TypeOrderBy.OrderByAscending:
                    return propertyStringFormat + " ASC";
                case TypeOrderBy.OrderByDescending:
                    return propertyStringFormat + " DESC";
            }

            throw new Exception("Order By Type NOT supported!");
        }
        
    }
}
