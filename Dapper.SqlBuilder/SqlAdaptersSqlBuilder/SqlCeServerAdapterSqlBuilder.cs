﻿using Dapper.SqlBuilder.Binder;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// This static class contains the method that extend Sql Server Compact Edition.
    /// </summary>
    public static class SqlCeServerAdapterSqlBuilder
    {

        /// <summary>
        /// Binding in the correct way the SQL query for Sql Server Compact Edition Database.
        /// </summary>
        /// <param name="sqlCeServerAdapter">The SqlCeServerAdapter object.</param>
        /// <param name="sqlBuilderBinder">SqlBuilderBinder object</param>
        /// <param name="queryParams">out list of object that contains params of Query</param>
        /// <returns>StringBuilder that contains the SQL query.</returns>
        public static StringBuilder SqlBuilderBindQuery(this SqlCeServerAdapter sqlCeServerAdapter, SqlBuilderBinder sqlBuilderBinder, out DynamicParameters queryParams)
        {
            throw new Exception("Method not implemented for Sql Server Compact Edition Database!");
        }

    }
}
