﻿using System;

namespace Dapper.SqlBuilder.Objects
{

    /// <summary>
    /// Rappresent the object that help to record in variables the Join Conditions
    /// </summary>
    public class JoinCondition
    {

        /// <summary>
        /// The type of the Model to join
        /// </summary>
        public Type TypeModelToJoin { get; set; }

        /// <summary>
        /// The condition of the Join
        /// </summary>
        public Condition ConditionToJoin { get; set; }

        /// <summary>
        /// The type of the join
        /// </summary>
        public TypeJoin TypeJoin { get; set; }
    }

}
