﻿using Dapper.DBMS.Extensions;

namespace Dapper.SqlBuilder.Objects
{

    /// <summary>
    /// Rappresent the Object for record the Order By in Variable
    /// </summary>
    public class OrderByInfo
    {

        /// <summary>
        /// Contains PropertyInfoExteded
        /// </summary>
        public PropertyInfoExtended Property { get; set; }

        /// <summary>
        /// Contains the Type of the Order By
        /// </summary>
        public TypeOrderBy TypeOrderBy { get; set; }

    }
}
