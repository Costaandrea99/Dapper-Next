﻿namespace Dapper.SqlBuilder.Objects
{

    /// <summary>
    /// Rappreset the Condition of a SQL Query
    /// </summary>
    public class Condition
    {
        /// <summary>
        /// Rappresent the left node of the Condition
        /// </summary>
        public NodeCondition Left { get; set; }

        /// <summary>
        /// Rappresent the Operand of the Condition
        /// </summary>
        public TypeOperandCondition Operand { get; set; }

        /// <summary>
        /// Rappresent the right node of the Condition
        /// </summary>
        public NodeCondition Right { get; set; }

    }
}
