﻿namespace Dapper.SqlBuilder.Objects
{

    /// <summary>
    /// Rappresent the Operand of a condition in a SQL Query
    /// </summary>
    public enum TypeOperandCondition
    {

        /// <summary>
        /// Rappresent the "=" Operand in a condition in a SQL Query
        /// </summary>
        Equal,

        /// <summary>
        /// Rappresent the "!=" Operand in a condition in a SQL Query
        /// </summary>
        NotEqual,

        /// <summary>
        /// Rappresent the "AND" Operand in a condition in a SQL Query
        /// </summary>
        And,

        /// <summary>
        /// Rappresent the "OR" Operand in a condition in a SQL Query
        /// </summary>
        Or,

        /// <summary>
        /// Rappresent the ">" Operand in a condition in a SQL Query
        /// </summary>
        GreaterThan,

        /// <summary>
        /// Rappresent the ">=" Operand in a condition in a SQL Query
        /// </summary>
        GreaterThanOrEqual,

        /// <summary>
        /// Rappresent the "less than" Operand in a condition in a SQL Query
        /// </summary>
        LessThan,

        /// <summary>
        /// Rappresent the "less than or =" Operand in a condition in a SQL Query
        /// </summary>
        LessThanOrEqual,

        /// <summary>
        /// Rappresent the "LIKE" Operand in a condition in a SQL Query
        /// </summary>
        Like,

        /// <summary>
        /// Rappresent the "LIKE" Operand in a condition in a SQL Query
        /// </summary>
        LikeStartWith,

        /// <summary>
        /// Rappresent the "LIKE" Operand in a condition in a SQL Query
        /// </summary>
        LikeBetweenWith,

        /// <summary>
        /// Rappresent the "LIKE" Operand in a condition in a SQL Query
        /// </summary>
        LikeEndWith
    }

    /// <summary>
    /// Rappresent the Type of NodeCondition
    /// </summary>
    public enum TypeNodeCondition
    {

        /// <summary>
        /// The Node Condition is a Property
        /// </summary>
        Property,

        /// <summary>
        /// The Node condition is a Constant
        /// </summary>
        Constant,

        /// <summary>
        /// The Node condition is another Condition
        /// </summary>
        Condition
    }

    /// <summary>
    /// Rappresent the different Type of Join
    /// </summary>
    public enum TypeJoin
    {

        /// <summary>
        /// Rappresent the "INNER JOIN" clause of SQL Query
        /// </summary>
        InnerJoin,

        /// <summary>
        /// Rappresent the "LEFT JOIN" clause of SQL Query
        /// </summary>
        LeftJoin,

        /// <summary>
        /// Rappresent the "RIGHT JOIN" clause of SQL Query
        /// </summary>
        RightJoin

    }

    /// <summary>
    /// Rappresent the different Type of Order By
    /// </summary>
    public enum TypeOrderBy
    {

        /// <summary>
        /// Rappresent "ORDER BY COLUMN ASC" clause in a SQL Query
        /// </summary>
        OrderByAscending,

        /// <summary>
        /// Rappresent "ORDER BY COLUMN DESC" clause in a SQL Query
        /// </summary>
        OrderByDescending

    }

}
