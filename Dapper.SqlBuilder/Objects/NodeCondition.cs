﻿using Dapper.DBMS.Extensions;

namespace Dapper.SqlBuilder.Objects
{

    /// <summary>
    /// This Object rappresent the "Node" of a condition of a SQL Query
    /// </summary>
    public class NodeCondition
    {

        /// <summary>
        /// The Constant value
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// The PropertyInfoExtendend 
        /// </summary>
        public PropertyInfoExtended Property { get; set; }

        /// <summary>
        /// The condition
        /// </summary>
        public Condition Condition { get; set; }

        /// <summary>
        /// Rappresent the type of the Node 
        /// </summary>
        public TypeNodeCondition Type { get; set; }

    }
}
