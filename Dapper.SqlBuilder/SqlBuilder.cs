﻿using Dapper.DBMS.Extensions;
using Dapper.SqlBuilder.Binder;
using Dapper.SqlBuilder.Mapper;
using Dapper.SqlBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Dapper.SqlBuilder
{

    /// <summary>
    /// SqlBuilder Class for manage query in every Database.
    /// </summary>
    public class SqlBuilder
    {

        #region Private Attributes 

        private SqlBuilderBinder Binder = new SqlBuilderBinder();

        #endregion


        /******************************************
         * 
         *  Method that rappresent SQL Functions
         *
         ******************************************/

        #region Select

        /// <summary>
        /// Set the "Select" part of SQL query, passing one Model and an array of object that contains the property of the Model.
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionSelectors">Array of object that contain the property of Select</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder Select<T>(Expression<Func<T, object[]>> expressionSelectors)
        {
            if (expressionSelectors == null) throw new Exception("expressionSelectors can't be null!");
            Binder.ListSelect.AddRange(GetPropertiesFromExpression(expressionSelectors.Body));
            return this;
        }

        /// <summary>
        /// Set the "Select" part of SQL query, passing two Model and an array of object that contains the property of the Models.
        /// </summary>
        /// <typeparam name="T1">Model 1</typeparam>
        /// <typeparam name="T2">Model 2</typeparam>
        /// <param name="expressionSelectors">Array of object that contain the property of Select</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder Select<T1, T2>(Expression<Func<T1, T2, object[]>> expressionSelectors)
        {
            if (expressionSelectors == null) throw new Exception("expressionSelectors can't be null!");
            Binder.ListSelect.AddRange(GetPropertiesFromExpression(expressionSelectors.Body));
            return this;
        }

        /// <summary>
        /// Set the "Select" part of SQL query, passing three Model and an array of object that contains the property of the Models.
        /// </summary>
        /// <typeparam name="T1">Model 1</typeparam>
        /// <typeparam name="T2">Model 2</typeparam>
        /// <typeparam name="T3">Model 3</typeparam>
        /// <param name="expressionSelectors">Array of object that contain the property of Select</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder Select<T1, T2, T3>(Expression<Func<T1, T2, T3, object[]>> expressionSelectors)
        {
            if (expressionSelectors == null) throw new Exception("expressionSelectors can't be null!");
            Binder.ListSelect.AddRange(GetPropertiesFromExpression(expressionSelectors.Body));
            return this;
        }

        /// <summary>
        /// Set the "Select" part of SQL query, passing one Model and an array of object that contains the property of the Model.
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionSelectors">Array of object that contain the property of Select</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder SelectDistinct<T>(Expression<Func<T, object[]>> expressionSelectors)
        {
            if (expressionSelectors == null) throw new Exception("expressionSelectors can't be null!");
            Binder.ListSelect.AddRange(GetPropertiesFromExpression(expressionSelectors.Body));
            Binder.Distinct = true;
            return this;
        }

        /// <summary>
        /// Set the "Select" part of SQL query, passing two Model and an array of object that contains the property of the Models.
        /// </summary>
        /// <typeparam name="T1">Model 1</typeparam>
        /// <typeparam name="T2">Model 2</typeparam>
        /// <param name="expressionSelectors">Array of object that contain the property of Select</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder SelectDistinct<T1, T2>(Expression<Func<T1, T2, object[]>> expressionSelectors)
        {
            if (expressionSelectors == null) throw new Exception("expressionSelectors can't be null!");
            Binder.ListSelect.AddRange(GetPropertiesFromExpression(expressionSelectors.Body));
            Binder.Distinct = true;
            return this;
        }

        /// <summary>
        /// Set the "Select" part of SQL query, passing three Model and an array of object that contains the property of the Models.
        /// </summary>
        /// <typeparam name="T1">Model 1</typeparam>
        /// <typeparam name="T2">Model 2</typeparam>
        /// <typeparam name="T3">Model 3</typeparam>
        /// <param name="expressionSelectors">Array of object that contain the property of Select</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder SelectDistinct<T1, T2, T3>(Expression<Func<T1, T2, T3, object[]>> expressionSelectors)
        {
            if (expressionSelectors == null) throw new Exception("expressionSelectors can't be null!");
            Binder.ListSelect.AddRange(GetPropertiesFromExpression(expressionSelectors.Body));
            Binder.Distinct = true;
            return this;
        }

        #endregion


        #region From

        /// <summary>
        /// Set the "From" part of SQL Query, passing the Models
        /// </summary>
        /// <param name="type">Type that rappresent the Model</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder From(Type type)
        {
            Binder.From = type ?? throw new Exception("Type can't be null!");
            return this;
        }

        #endregion


        #region Joins | InnerJoin - LeftJoin - RightJoin

        /// <summary>
        /// Set the "InnerJoin" part of SQL Query, passing the type of the model to join and the expression of the condition
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <param name="typeModelToJoin">Type of the model to join</param>
        /// <param name="expressionCondition">The expression of the condition of the inner join</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder InnerJoin<T1, T2>(Type typeModelToJoin, Expression<Func<T1, T2, bool>> expressionCondition)
        {
            Binder.ListJoinConditions.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.InnerJoin,
                TypeModelToJoin = typeModelToJoin,
                ConditionToJoin = GetCondition(expressionCondition.Body)
            });
            return this;
        }

        /// <summary>
        /// Set the "LeftJoin" part of SQL Query, passing the type of the model to join and the expression of the condition
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <param name="typeModelToJoin">Type of the model to join</param>
        /// <param name="expressionCondition">The expression of the condition of the left join</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder LeftJoin<T1, T2>(Type typeModelToJoin, Expression<Func<T1, T2, bool>> expressionCondition)
        {
            Binder.ListJoinConditions.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.LeftJoin,
                TypeModelToJoin = typeModelToJoin,
                ConditionToJoin = GetCondition(expressionCondition.Body)
            });
            return this;
        }

        /// <summary>
        /// Set the "RightJoin" part of SQL Query, passing the type of the model to join and the expression of the condition
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <param name="typeModelToJoin">Type of the model to join</param>
        /// <param name="expressionCondition">The expression of the condition of the right join</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder RightJoin<T1, T2>(Type typeModelToJoin, Expression<Func<T1, T2, bool>> expressionCondition)
        {
            Binder.ListJoinConditions.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.LeftJoin,
                TypeModelToJoin = typeModelToJoin,
                ConditionToJoin = GetCondition(expressionCondition.Body)
            });
            return this;
        }

        #endregion


        #region Where

        /// <summary>
        /// Set the "Where" part of SQL Query, passing the expression
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionCondition">The expression of the condition</param>
        /// <returns>SqlBuilder Object</returns>
        public SqlBuilder Where<T>(Expression<Func<T, bool>> expressionCondition)
        {
            if (expressionCondition == null) throw new Exception("expressionCondition can't be null!");
            SetWhereCondition(GetCondition(expressionCondition.Body));
            return this;
        }

        /// <summary>
        /// Set the "Where" part of SQL Query, passing the expression
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <param name="expressionCondition">The expression of the condition</param>
        /// <returns>SqlBuilder Object</returns>
        public SqlBuilder Where<T1, T2>(Expression<Func<T1, T2, bool>> expressionCondition)
        {
            if (expressionCondition == null) throw new Exception("expressionCondition can't be null!");
            SetWhereCondition(GetCondition(expressionCondition.Body));
            return this;
        }

        /// <summary>
        /// Set the "Where" part of SQL Query, passing the expression
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <typeparam name="T3">Model</typeparam>
        /// <param name="expressionCondition">The expression of the condition</param>
        /// <returns>SqlBuilder Object</returns>
        public SqlBuilder Where<T1, T2, T3>(Expression<Func<T1, T2, T3, bool>> expressionCondition)
        {
            if (expressionCondition == null) throw new Exception("expressionCondition can't be null!");
            SetWhereCondition(GetCondition(expressionCondition.Body));
            return this;
        }

        #endregion


        #region Like Statement

        /// <summary>
        /// Set the "Like" part of SQL Query, passing the selector and the string
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionSelector">Expression of selector</param>
        /// <param name="likeString">String to Like</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder Like<T>(Expression<Func<T, object>> expressionSelector, string likeString)
        {
            SetWhereCondition(new Condition()
            {
                Left = new NodeCondition()
                {
                    Property = GetPropertyFromExpression(expressionSelector.Body),
                    Type = TypeNodeCondition.Property
                },
                Operand = TypeOperandCondition.Like,
                Right = new NodeCondition()
                {
                    Value = likeString,
                    Type = TypeNodeCondition.Constant
                }
            });

            return this;
        }

        /// <summary>
        /// Set the "Like" part of SQL Query, passing the selector and the string
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionSelector">Expression of selector</param>
        /// <param name="likeString">String to Like</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder LikeStartWith<T>(Expression<Func<T, object>> expressionSelector, string likeString)
        {
            SetWhereCondition(new Condition()
            {
                Left = new NodeCondition()
                {
                    Property = GetPropertyFromExpression(expressionSelector.Body),
                    Type = TypeNodeCondition.Property
                },
                Operand = TypeOperandCondition.LikeStartWith,
                Right = new NodeCondition()
                {
                    Value = likeString,
                    Type = TypeNodeCondition.Constant
                }
            });

            return this;
        }

        /// <summary>
        /// Set the "Like" part of SQL Query, passing the selector and the string
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionSelector">Expression of selector</param>
        /// <param name="likeString">String to Like</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder LikeBetweenWith<T>(Expression<Func<T, object>> expressionSelector, string likeString)
        {
            SetWhereCondition(new Condition()
            {
                Left = new NodeCondition()
                {
                    Property = GetPropertyFromExpression(expressionSelector.Body),
                    Type = TypeNodeCondition.Property
                },
                Operand = TypeOperandCondition.LikeBetweenWith,
                Right = new NodeCondition()
                {
                    Value = likeString,
                    Type = TypeNodeCondition.Constant
                }
            });

            return this;
        }

        /// <summary>
        /// Set the "Like" part of SQL Query, passing the selector and the string
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionSelector">Expression of selector</param>
        /// <param name="likeString">String to Like</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder LikeEndWith<T>(Expression<Func<T, object>> expressionSelector, string likeString)
        {
            SetWhereCondition(new Condition()
            {
                Left = new NodeCondition()
                {
                    Property = GetPropertyFromExpression(expressionSelector.Body),
                    Type = TypeNodeCondition.Property
                },
                Operand = TypeOperandCondition.LikeEndWith,
                Right = new NodeCondition()
                {
                    Value = likeString,
                    Type = TypeNodeCondition.Constant
                }
            });

            return this;
        }

        #endregion


        #region OrderBy

        /// <summary>
        /// Set the "Order By ASC" part of SQL Query
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionSelectors"></param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder OrderByAscending<T>(Expression<Func<T, object[]>> expressionSelectors)
        {
            List<PropertyInfoExtended> listProperty = GetPropertiesFromExpression(expressionSelectors.Body);
            foreach (PropertyInfoExtended property in listProperty)
                SetOrderBy(property, TypeOrderBy.OrderByAscending);
            return this;
        }

        /// <summary>
        /// Set the "Order By ASC" part of SQL Query
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <param name="expressionSelectors"></param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder OrderByAscending<T1, T2>(Expression<Func<T1, T2, object[]>> expressionSelectors)
        {
            List<PropertyInfoExtended> listProperty = GetPropertiesFromExpression(expressionSelectors.Body);
            foreach (PropertyInfoExtended property in listProperty)
                SetOrderBy(property, TypeOrderBy.OrderByAscending);
            return this;
        }

        /// <summary>
        /// Set the "Order By ASC" part of SQL Query
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <typeparam name="T3">Model</typeparam>
        /// <param name="expressionSelectors"></param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder OrderByAscending<T1, T2, T3>(Expression<Func<T1, T2, T3, object[]>> expressionSelectors)
        {
            List<PropertyInfoExtended> listProperty = GetPropertiesFromExpression(expressionSelectors.Body);
            foreach (PropertyInfoExtended property in listProperty)
                SetOrderBy(property, TypeOrderBy.OrderByAscending);
            return this;
        }

        /// <summary>
        /// Set the "Order By DESC" part of SQL Query
        /// </summary>
        /// <typeparam name="T">Model</typeparam>
        /// <param name="expressionSelectors"></param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder OrderByDescending<T>(Expression<Func<T, object[]>> expressionSelectors)
        {
            List<PropertyInfoExtended> listProperty = GetPropertiesFromExpression(expressionSelectors.Body);
            foreach (PropertyInfoExtended property in listProperty)
                SetOrderBy(property, TypeOrderBy.OrderByAscending);
            return this;
        }

        /// <summary>
        /// Set the "Order By DESC" part of SQL Query
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <param name="expressionSelectors"></param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder OrderByDescending<T1, T2>(Expression<Func<T1, T2, object[]>> expressionSelectors)
        {
            List<PropertyInfoExtended> listProperty = GetPropertiesFromExpression(expressionSelectors.Body);
            foreach (PropertyInfoExtended property in listProperty)
                SetOrderBy(property, TypeOrderBy.OrderByAscending);
            return this;
        }

        /// <summary>
        /// Set the "Order By DESC" part of SQL Query
        /// </summary>
        /// <typeparam name="T1">Model</typeparam>
        /// <typeparam name="T2">Model</typeparam>
        /// <typeparam name="T3">Model</typeparam>
        /// <param name="expressionSelectors"></param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder OrderByDescending<T1, T2, T3>(Expression<Func<T1, T2, T3, object[]>> expressionSelectors)
        {
            List<PropertyInfoExtended> listProperty = GetPropertiesFromExpression(expressionSelectors.Body);
            foreach (PropertyInfoExtended property in listProperty)
                SetOrderBy(property, TypeOrderBy.OrderByAscending);
            return this;
        }

        #endregion


        #region Limit / Offset

        /// <summary>
        /// Set the "LIMIT" part of SQL Query
        /// </summary>
        /// <param name="limit">Integer that rappresent the Limit</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder Limit(int? limit)
        {
            Binder.Limit = limit;
            return this;
        }

        /// <summary>
        /// Set the "OFFSET" part of SQL Query
        /// </summary>
        /// <param name="offset">Integer that rappresent the Offset</param>
        /// <returns>SqlBuilder object</returns>
        public SqlBuilder Offset(int? offset)
        {
            Binder.Offset = offset;
            return this;
        }

        #endregion


        /****************************************
         * 
         *  Method about Binding of SQL string
         *
         ****************************************/

        #region Bind Method

        /// <summary>
        /// Binding the SQLBuilder
        /// </summary>
        /// <param name="connection">Db Connection</param>
        /// <param name="queryParams"></param>
        /// <returns>String that contain the sql query</returns>
        public string BindStringSql(IDbConnection connection, out DynamicParameters queryParams)
        {
            return Binder.BindSql(connection, out queryParams).ToString();
        }

        /// <summary>
        /// Bind the Sql String and execute the query returned the list of Model
        /// </summary>
        /// <typeparam name="T">Generic Type of Model</typeparam>
        /// <param name="dbConnection">Db Connection</param>
        /// <returns>List of Generic Type passed</returns>
        public IEnumerable<T> BindStringAndExecuteQuery<T>(IDbConnection dbConnection)
        {
            // On the "BindSql" I control that the "FROM" of SQL is not null! If is null I generate Exception
            string sqlQuery = Binder.BindSql(dbConnection, out DynamicParameters queryParams).ToString();

            // If i haven't joins [Normal Query]
            if (Binder.ListJoinConditions.Count == 0)
                return (IEnumerable<T>)dbConnection.Query(sqlQuery, queryParams);
            else
                return SqlBuilderMapper.AutoMappingBasedOnBinder<T>(Binder, dbConnection, sqlQuery, queryParams);
        }

        #endregion


        #region Private Methods

        private List<PropertyInfoExtended> GetPropertiesFromExpression(Expression expression)
        {
            List<PropertyInfoExtended> props = new List<PropertyInfoExtended>();

            if (expression is NewArrayExpression arrayExpressions)
            {
                foreach (Expression itemExpression in arrayExpressions.Expressions)
                    props.Add(GetPropertyFromExpression(itemExpression));
            }
            else
                props.Add(GetPropertyFromExpression(expression));

            return props;
        }
        private PropertyInfoExtended GetPropertyFromExpression(Expression expression)
        {
            if (expression is MemberExpression memberExpression) return new PropertyInfoExtended((PropertyInfo)memberExpression.Member);
            if (expression is UnaryExpression unaryExpression) return GetPropertyFromExpression(unaryExpression.Operand);

            throw new Exception("This type of expression is not implemented yet!");
        }
        private void SetWhereCondition(Condition condition)
        {
            if (Binder.WhereCondition != null)
            {
                Binder.WhereCondition = new Condition()
                {
                    Left = new NodeCondition()
                    {
                        Condition = Binder.WhereCondition,
                        Type = TypeNodeCondition.Condition
                    },
                    Operand = TypeOperandCondition.And,
                    Right = new NodeCondition()
                    {
                        Condition = condition,
                        Type = TypeNodeCondition.Condition
                    }
                };
            }
            else
                Binder.WhereCondition = condition;
        }
        private Condition GetCondition(Expression expression)
        {
            Condition condition = new Condition();

            if (expression is MethodCallExpression) throw new Exception("Not implemented yet the conversion of a Method!");

            Expression leftCondition = GetLeftNode(expression);
            Expression rightCondition = GetRightNode(expression);
            condition.Left = GetNodeCondition(leftCondition);
            condition.Right = GetNodeCondition(rightCondition);
            condition.Operand = GetOperand(expression.NodeType);

            return condition;
        }
        private TypeOperandCondition GetOperand(ExpressionType expressionType)
        {
            switch (expressionType)
            {
                case ExpressionType.Equal: return TypeOperandCondition.Equal;
                case ExpressionType.NotEqual: return TypeOperandCondition.NotEqual;
                case ExpressionType.And: return TypeOperandCondition.And;
                case ExpressionType.AndAlso: return TypeOperandCondition.And;
                case ExpressionType.Or: return TypeOperandCondition.Or;
                case ExpressionType.OrElse: return TypeOperandCondition.Or;
                case ExpressionType.GreaterThan: return TypeOperandCondition.GreaterThan;
                case ExpressionType.GreaterThanOrEqual: return TypeOperandCondition.GreaterThanOrEqual;
                case ExpressionType.LessThan: return TypeOperandCondition.LessThan;
                case ExpressionType.LessThanOrEqual: return TypeOperandCondition.LessThanOrEqual;
            }

            throw new Exception("This expression type is not implemented yet!");
        }
        private NodeCondition GetNodeCondition(Expression expression)
        {
            NodeCondition nodeCondition = new NodeCondition();

            if (expression is MemberExpression memberExpression)
            {
                nodeCondition.Property = new PropertyInfoExtended((PropertyInfo)memberExpression.Member);
                nodeCondition.Type = TypeNodeCondition.Property;
            }
            else if (expression is ConstantExpression constantExpression)
            {
                nodeCondition.Value = constantExpression.Value;
                nodeCondition.Type = TypeNodeCondition.Constant;
            }
            else if (expression is BinaryExpression binaryExpression)
            {
                nodeCondition.Condition = GetCondition(binaryExpression);
                nodeCondition.Type = TypeNodeCondition.Condition;
            }

            return nodeCondition;
        }
        private Expression GetLeftNode(Expression expression)
        {
            dynamic exp = expression;
            return ((Expression)exp.Left);
        }
        private Expression GetRightNode(Expression expression)
        {
            dynamic exp = expression;
            return ((Expression)exp.Right);
        }
        private void SetOrderBy(PropertyInfoExtended property, TypeOrderBy typeOrderBy)
        {
            Binder.ListOrderBy.Add(new OrderByInfo()
            {
                Property = property,
                TypeOrderBy = typeOrderBy
            });
        }

        #endregion

    }
}
