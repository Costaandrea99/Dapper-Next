﻿using Dapper.DBMS.Cache;
using Dapper.DBMS.Extensions;
using Dapper.DBMS.SqlAdapters;
using Dapper.SqlBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Dapper.SqlBuilder.Binder
{

    /// <summary>
    /// This class contains the information for building the SqlQuery
    /// </summary>
    public class SqlBuilderBinder
    {

        /// <summary>
        /// If is checked the query contains "Distinct" value.
        /// </summary>
        public bool Distinct = false;

        /// <summary>
        /// If is not null, set the LIMIT of result of the query
        /// </summary>
        public int? Limit = null;

        /// <summary>
        /// If is not null, set the OFFSET of result of the query
        /// </summary>
        public int? Offset = null;

        /// <summary>
        /// List with the property of the models that you want in the SELECT statement of the query
        /// </summary>
        public List<PropertyInfoExtended> ListSelect = new List<PropertyInfoExtended>();

        /// <summary>
        /// Type of the model that you want to extract data
        /// </summary>
        public Type From = null;

        /// <summary>
        /// List with the information for set the JOIN state of the query
        /// </summary>
        public List<JoinCondition> ListJoinConditions = new List<JoinCondition>();

        /// <summary>
        /// Condition of the WHERE state of the query.
        /// </summary>
        public Condition WhereCondition = null;

        /// <summary>
        /// List with the information for set the ORDER BY state of the query
        /// </summary>
        public List<OrderByInfo> ListOrderBy = new List<OrderByInfo>();

        /// <summary>
        /// This Method bind the Sql string based on the correct adapter!
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="queryParams"></param>
        /// <returns>Stringbuilder that contains the SQL</returns>
        public StringBuilder BindSql(IDbConnection connection, out DynamicParameters queryParams)
        {
            if (From == null) throw new Exception("FROM clause can't be empty!");
            var adapter = StaticCache.GetAdapter(connection);
            return adapter.SqlBuilderBindQuery(this, out queryParams);
        }

    }
}
