﻿using Dapper.SqlBuilder.Binder;
using Dapper.SqlBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dapper.SqlBuilder.Utils
{

    /// <summary>
    /// Static class that contain utils method for binding query with different Database
    /// </summary>
    public static class Utils
    {

        /// <summary>
        /// Create Dictionary of Alias Name of the Models
        /// </summary>
        /// <param name="sqlBuilderBinder">SqlBuilderBinder object</param>
        /// <returns>Dictionary that contain the value</returns>
        public static Dictionary<Type, string> GetDictionaryModelAlias(SqlBuilderBinder sqlBuilderBinder)
        {
            int indexAliasNameArray = 0;
            Dictionary<Type, string> dictionaryModelAlias = new Dictionary<Type, string>();

            // Add "From" 
            dictionaryModelAlias.Add(sqlBuilderBinder.From, "T" + indexAliasNameArray);
            indexAliasNameArray++;

            // Add Table Joined
            foreach(JoinCondition joinCondition in sqlBuilderBinder.ListJoinConditions)
            {
                dictionaryModelAlias.Add(joinCondition.TypeModelToJoin, "T" + indexAliasNameArray);
                indexAliasNameArray++;
            }

            return dictionaryModelAlias;
        }

        /// <summary>
        /// Get the alias names of the Model
        /// </summary>
        /// <param name="dictionaryModelAlias">Dictionary with alias names</param>
        /// <param name="modelType">Type Model to Find</param>
        /// <returns></returns>
        public static string GetModelAliasNameFromDictionary(this Dictionary<Type, string> dictionaryModelAlias, Type modelType)
        {
            return dictionaryModelAlias[modelType];
        }

        /// <summary>
        /// Add dynamic parameters with dynamic name
        /// </summary>
        /// <param name="dynamicParametersIn"></param>
        /// <param name="value"></param>
        /// <param name="dynamicParametersOut"></param>
        /// <returns></returns>
        public static string AddDynamicParam(this DynamicParameters dynamicParametersIn, object value, out DynamicParameters dynamicParametersOut)
        {
            string nameParam = "";
            bool continueToSearch = true;
            int indexParam = dynamicParametersIn.ParameterNames.ToList().Count;

            do
            {
                var findParam = dynamicParametersIn.ParameterNames.ToList().Find(f => f == "@P" + indexParam);
                if(findParam == null)
                {
                    nameParam = "@P" + indexParam;
                    dynamicParametersIn.Add(nameParam, value);
                    continueToSearch = false;
                }
            }
            while (continueToSearch);

            dynamicParametersOut = dynamicParametersIn;
            return nameParam;
        }

    }
}
