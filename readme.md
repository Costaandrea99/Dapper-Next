# Dapper-Next: extends and improves performance of Dapper

Dapper-Next is a project based on Dapper library, that extends and improves the performance of the original project.<br />
Only the "Dapper" solution and part of the "Dapper.Contrib" solution, which has been extended with new methods, has been retained in the main project.

## Solutions present in the project:

### Dapper

"a simple object mapper for .Net"<br />
The original source code has been retained. ([Original project](https://github.com/StackExchange/Dapper))

### Dapper.DBMS ([Github](https://github.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.DBMS) - [Gitlab](https://gitlab.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.DBMS))

Dapper.DBMS contains all feature that improve Database mapping like:

* Custom attributes for models and their properties.
* Caching of queries and Model-Properties-Types
* Database Adapter.
* Extension about some objects and Proxy Mapper.

Dependency: Dapper.

### Dapper.Contrib ([Github](https://github.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.Contrib) - [Gitlab](https://gitlab.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.Contrib))

Dapper.Contrib contains a number of helper methods for inserting, getting, updating and deleting records using directly the objects that reppresent the models.

Dependency: Dapper, Dapper.DBMS.

### Dapper.SqlBuilder ([Github](https://github.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.SqlBuilder) - [Gitlab](https://gitlab.com/FabrizioPairone/Dapper-Next/tree/master/Dapper.SqlBuilder))

Dapper.SqlBuilder contains methods (like LINQ) that represent the SQL commands that allow you to always get the query translated into the correct SQL syntax for the database that you are using.<br />
There are two methods for binding the query:
- The first one return the string with the SQL query.
- The second directly bind the query, execute it, get the result and execute auto-mapping based on the T object that will be returned

Dependency: Dapper, Dapper.DBMS.

<hr />

For each solution it is recommended to read the relative readme.md file.

<hr />

## Database Supported

* FireBird
* MySql
* Postgresql
* SQL Lite
* SQL Server 
* SQL Server Compact Edition


## What has not been implemented and future additions

* Dapper.SqlBuilder future additions:
    - Mapping dynamically one to many
    - A better version for management dynamic parameters in query.
    - A better version for mapping Type Model with as name. <br /> (Now works only when the we using always different tables)

* Dapper.SqlBuilder methods not implemented yet:
    - Dynamic mapping of Right Join


## Authors

* The "Dapper" base project is created by [Sam Saffron](https://github.com/SamSaffron), [Marc Gravell](https://github.com/mgravell) and [Nick Craver](https://github.com/NickCraver) (Check it [Here](https://github.com/StackExchange/Dapper))
* The "Dapper.DBMS" project is created by [Fabrizio Pairone](https://github.com/FabrizioPairone) ([Gitlab](https://gitlab.com/FabrizioPairone)) and it's inspiring at some function of Dapper.Contrib that have been generalized.
* The "Dapper.Contrib" project is created by [Sam Saffron](https://github.com/SamSaffron) and [Johan Danforth](https://github.com/johandanforth). (Check the original version [here](https://github.com/StackExchange/Dapper/tree/master/Dapper.Contrib))
* The "Dapper.SqlBuilder" project is create by [Fabrizio Pairone](https://github.com/FabrizioPairone) ([Gitlab](https://gitlab.com/FabrizioPairone)).


## Contributors

There aren't contributors yet.


## License

MIT License

