﻿using System;

namespace Dapper.DBMS.Attributes
{

    /// <summary>
    /// Enum that rappresent the type of Computed Columns
    /// </summary>
    public enum TypeComputed
    {

        /// <summary>
        /// Rappresent that column value is defined by AVG() SQL command.
        /// </summary>
        AVG,

        /// <summary>
        /// Rappresent that column value is defined by COUNT() SQL command.
        /// </summary>
        COUNT,

        /// <summary>
        /// Rappresent that column value is defined by MAX() SQL command.
        /// </summary>
        MAX,

        /// <summary>
        /// Rappresent that column value is defined by MIN() SQL command.
        /// </summary>
        MIN,

        /// <summary>
        /// Rappresent that column value is defined by SUM() SQL command.
        /// </summary>
        SUM

    }

    /// <summary>
    /// Specifies that this is a computed column.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ComputedAttribute : Attribute
    {

        /// <summary>
        /// Rappresent the Type of Computed Column
        /// </summary>
        public TypeComputed TypeComputed { get; set; }

        /// <summary>
        /// Constructor of Computed Attribute
        /// </summary>
        /// <param name="typeComputed">Type of Computed Column</param>
        public ComputedAttribute(TypeComputed typeComputed)
        {
            TypeComputed = TypeComputed;
        }

    }
}
