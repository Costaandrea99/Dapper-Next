﻿using System;

namespace Dapper.DBMS.Attributes
{

    /// <summary>
    /// Specifies that this column have to be ignored on Update execution of Dapper.Contrib
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreInUpdateAttribute : Attribute { }

}
