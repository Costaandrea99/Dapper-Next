﻿using System;

namespace Dapper.DBMS.Attributes
{

    /// <summary>
    /// Defines the name of a table to use in Dapper.Contrib commands.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {

        /// <summary>
        /// The name of the table in the database
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The name of the view associated at the Table
        /// </summary>
        public string View { get; set; }
        
        
        /// <summary>
        /// Creates a table mapping to a specific name for Dapper.Contrib commands
        /// </summary>
        /// <param name="tableName">The name of this table in the database.</param>
        /// <param name="viewName">The name of the view in db associated at this table.</param>
        public TableAttribute(string tableName, string viewName = "")
        {
            Name = tableName;
            View = viewName;
        }

    }
}
