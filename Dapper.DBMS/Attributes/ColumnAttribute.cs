﻿using System;

namespace Dapper.DBMS.Attributes
{

    /// <summary>
    /// Defines the name of a column to use in Dapper.Contrib commands
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {

        /// <summary>
        /// The name of the column in the database
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Creates a column mapping to a specific name for Dapper.Contrib commands
        /// </summary>
        /// <param name="columnName">The name of this column in the database.</param>
        public ColumnAttribute(string columnName)
        {
            Name = columnName;
        }

    }
}
