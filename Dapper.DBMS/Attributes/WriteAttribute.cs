﻿using System;

namespace Dapper.DBMS.Attributes
{

    /// <summary>
    /// Specifies whether a field is writable in the database.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class WriteAttribute : Attribute
    {

        /// <summary>
        /// Whether a field is writable in the database.
        /// </summary>
        public bool Write { get; }

        /// <summary>
        /// Specifies whether a field is writable in the database.
        /// </summary>
        /// <param name="write">Whether a field is writable in the database.</param>
        public WriteAttribute(bool write)
        {
            Write = write;
        }

    }
}
