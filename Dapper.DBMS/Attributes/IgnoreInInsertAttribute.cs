﻿using System;

namespace Dapper.DBMS.Attributes
{

    /// <summary>
    /// Specifies that this column have to be ignored on Insert execution of Dapper.Contrib
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreInInsertAttribute : Attribute { }

}
