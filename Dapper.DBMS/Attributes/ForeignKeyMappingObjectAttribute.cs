﻿using System;

namespace Dapper.DBMS.Attributes
{

    /// <summary>
    /// Mapping that this object is not in Table but refence to another Model, that you can use for Joins
    /// </summary>
    public class ForeignKeyMappingObjectAttribute : Attribute
    {

        /// <summary>
        /// The Name of the property that have external ID
        /// </summary>
        public string PropertyName { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="propertyName">The name of the property that have external ID</param>
        public ForeignKeyMappingObjectAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }

    }
}
