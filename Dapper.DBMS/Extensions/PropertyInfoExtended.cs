﻿using Dapper.DBMS.Attributes;
using Dapper.DBMS.Cache;
using System.Linq;
using System.Reflection;

namespace Dapper.DBMS.Extensions
{

    /// <summary>
    /// Extension of the class PropertyInfo, with the possibility to set the column name.
    /// </summary>
    public class PropertyInfoExtended
    {

        #region Attributes

        /// <summary>
        /// Property Info that it is extended
        /// </summary>
        public PropertyInfo Property { get; set; }

        /// <summary>
        /// Property Info Column Name Mapping
        /// </summary>
        public string ColumnName { get; private set; }

        /// <summary>
        /// Property Info Table Name Mapping
        /// </summary>
        public string TableName { get; private set; }

        #endregion


        #region Constructor

        /// <summary>
        /// Constructor of PropertyInfoExtended
        /// </summary>
        /// <param name="property">PropertyInfo object that you want extended</param>
        public PropertyInfoExtended(PropertyInfo property)
        {
            Property = property;
            ColumnName = GetColumName(property);
            TableName = StaticCache.GetTableName(property.DeclaringType);
        }

        #endregion


        #region Private Method

        private string GetColumName(PropertyInfo property)
        {
            var columnName = property.GetCustomAttributes(false).SingleOrDefault(attr => attr.GetType() == typeof(ColumnAttribute)) as dynamic;
            if (columnName != null) return columnName.Name;
            return property.Name;
        }

        #endregion

    }
}
