﻿using Dapper.DBMS.Attributes;
using Dapper.DBMS.Extensions;
using Dapper.DBMS.SqlAdapters;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Dapper.DBMS.Cache
{

    /// <summary>
    /// Static Class with static var for Chacing.
    /// </summary>
    public static class StaticCache
    {

        #region Public Attributes

        /// <summary>
        /// The function to get a database type from the given <see cref="IDbConnection"/>.
        /// </summary>
        /// <param name="connection">The connection to get a database type name from.</param>
        public delegate string GetDatabaseTypeDelegate(IDbConnection connection);

        /// <summary>
        /// Specifies a custom callback that detects the database type instead of relying on the default strategy (the name of the connection type object).
        /// Please note that this callback is global and will be used by all the calls that require a database specific adapter.
        /// </summary>
        public static GetDatabaseTypeDelegate GetDatabaseType;

        #endregion


        #region Private Attributes

        private static readonly SqlAdapter DefaultAdapter = new PostgresAdapter();

        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>> TypeProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>> KeyProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>> ExplicitKeyProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>> ComputedProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>> IgnoreInInsertProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>> IgnoreInUpdateProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>> ForeignKeyMappingObjectProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfoExtended>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, string> GetQueries = new ConcurrentDictionary<RuntimeTypeHandle, string>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, string> TypeTableName = new ConcurrentDictionary<RuntimeTypeHandle, string>();
        private static readonly Dictionary<string, SqlAdapter> AdapterDictionary = new Dictionary<string, SqlAdapter>
        {
            ["sqlconnection"] = new SqlServerAdapter(),
            ["sqlceconnection"] = new SqlCeServerAdapter(),
            ["npgsqlconnection"] = new PostgresAdapter(),
            ["sqliteconnection"] = new SQLiteAdapter(),
            ["mysqlconnection"] = new MySqlAdapter(),
            ["fbconnection"] = new FbAdapter()
        };

        #endregion


        #region Public Methods

        /// <summary>
        /// Get the correct table name!
        /// </summary>
        /// <param name="type">Type of Model</param>
        /// <returns></returns>
        public static string GetTableName(Type type)
        {
            if (TypeTableName.TryGetValue(type.TypeHandle, out string name)) return name;

            //NOTE: This as dynamic trick should be able to handle both our own Table-attribute as well as the one in EntityFramework 
            var tableAttr = type
            #if NETSTANDARD1_3
                .GetTypeInfo()
            #endif
            .GetCustomAttributes(false).SingleOrDefault(attr => attr.GetType() == typeof(TableAttribute)) as dynamic;

            name = (tableAttr == null ? type.Name : (!string.IsNullOrEmpty(tableAttr.View) ? tableAttr.View : tableAttr.Name));
            TypeTableName[type.TypeHandle] = name;

            return name;
        }

        /// <summary>
        /// Get the query of Get
        /// </summary>
        /// <param name="type"></param>
        /// <param name="method"></param>
        /// <param name="cacheType"></param>
        /// <returns></returns>
        public static string GetQuery(Type type, string method, Type cacheType = null)
        {
            cacheType = (cacheType ?? type);
            if (GetQueries.TryGetValue(cacheType.TypeHandle, out string sql)) return sql;

            var key = GetSingleKey<Type>(method);
            var name = GetTableName(type);

            sql = $"select * from {name} where {key.ColumnName} = @id";
            GetQueries[cacheType.TypeHandle] = sql;

            return sql;
        }

        /// <summary>
        /// Get the type of DB base on the connection
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <returns></returns>
        public static SqlAdapter GetAdapter(IDbConnection connection)
        {
            var name = GetDatabaseType?.Invoke(connection).ToLower()
                       ?? connection.GetType().Name.ToLower();

            return !AdapterDictionary.ContainsKey(name)
                ? DefaultAdapter
                : AdapterDictionary[name];
        }

        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<PropertyInfoExtended> TypePropertiesCache(Type type)
        {
            if (TypeProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfoExtended> propertyExtended))
                return propertyExtended.ToList();

            var properties = type.GetProperties().Where(IsWriteable).ToArray();
            var propertiesExtended = properties.Select(s => new PropertyInfoExtended(s)).ToArray();
            TypeProperties[type.TypeHandle] = propertiesExtended;
            return propertiesExtended.ToList();
        }

        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the Primary Key
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<PropertyInfoExtended> KeyPropertiesCache(Type type)
        {
            if (KeyProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfoExtended> propertyExtended))
                return propertyExtended.ToList();

            var allProperties = TypePropertiesCache(type);
            var keyProperties = allProperties.Where(p => p.Property.GetCustomAttributes(true).Any(a => a is KeyAttribute)).ToList();

            if (keyProperties.Count == 0)
            {
                var idProp = allProperties.Find(p => string.Equals(p.Property.Name, "id", StringComparison.CurrentCultureIgnoreCase));
                if (idProp != null && !idProp.Property.GetCustomAttributes(true).Any(a => a is ExplicitKeyAttribute))
                    keyProperties.Add(idProp);
            }

            KeyProperties[type.TypeHandle] = keyProperties;
            return keyProperties;
        }

        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the Primary Key Explicit
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<PropertyInfoExtended> ExplicitKeyPropertiesCache(Type type)
        {
            if (ExplicitKeyProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfoExtended> propertyExtended))
                return propertyExtended.ToList();

            var explicitKeyProperties = TypePropertiesCache(type).Where(p => p.Property.GetCustomAttributes(true).Any(a => a is ExplicitKeyAttribute)).ToList();
            ExplicitKeyProperties[type.TypeHandle] = explicitKeyProperties;
            return explicitKeyProperties;
        }

        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the Computed Properties [Column that are a result of count or something]
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<PropertyInfoExtended> ComputedPropertiesCache(Type type)
        {
            if (ComputedProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfoExtended> propertyExtended))
                return propertyExtended.ToList();

            var computedProperties = TypePropertiesCache(type).Where(p => p.Property.GetCustomAttributes(true).Any(a => a is ComputedAttribute)).ToList();
            ComputedProperties[type.TypeHandle] = computedProperties;
            return computedProperties;
        }

        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the Propertis to ingnore in insert
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<PropertyInfoExtended> IgnoreInInsertPropertiesCache(Type type)
        {
            if (IgnoreInInsertProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfoExtended> propertyExtended))
                return propertyExtended.ToList();

            var ignoreInInsertProperty = TypePropertiesCache(type).Where(p => p.Property.GetCustomAttributes(true).Any(a => a is IgnoreInInsertAttribute)).ToList();
            IgnoreInInsertProperties[type.TypeHandle] = ignoreInInsertProperty;
            return ignoreInInsertProperty;
        }

        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the properties to ignore in update
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<PropertyInfoExtended> IgnoreInUpdatePropertiesCache(Type type)
        {
            if (IgnoreInUpdateProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfoExtended> propertyExtended))
                return propertyExtended.ToList();

            var ignoreInUpdateProperty = TypePropertiesCache(type).Where(p => p.Property.GetCustomAttributes(true).Any(a => a is IgnoreInUpdateAttribute)).ToList();
            IgnoreInUpdateProperties[type.TypeHandle] = ignoreInUpdateProperty;
            return ignoreInUpdateProperty;
        }

        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the properties that have ForeingKeyMappingObject
        /// </summary>
        /// <param name="type">Type of the model</param>
        /// <returns>List of property info extended</returns>
        public static List<PropertyInfoExtended> ForeignKeyMappingObjectPropertiesCache(Type type)
        {
            if (ForeignKeyMappingObjectProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfoExtended> propertyExtended))
                return propertyExtended.ToList();

            var foreignKeyMappingObjectProperty = TypePropertiesCache(type).Where(p => p.Property.GetCustomAttributes(true).Any(a => a is ForeignKeyMappingObjectAttribute)).ToList();
            ForeignKeyMappingObjectProperties[type.TypeHandle] = foreignKeyMappingObjectProperty;
            return foreignKeyMappingObjectProperty;
        }

        /// <summary>
        /// Check if the property is Writeable or not
        /// </summary>
        /// <param name="pi"></param>
        /// <returns></returns>
        public static bool IsWriteable(PropertyInfo pi)
        {
            var attributes = pi.GetCustomAttributes(typeof(WriteAttribute), false).AsList();
            if (attributes.Count != 1) return true;
            return ((WriteAttribute)attributes[0]).Write;
        }
        /// <summary>
        /// Check if the property is Writeable or not
        /// </summary>
        /// <param name="pie"></param>
        /// <returns></returns>
        public static bool IsWriteable(PropertyInfoExtended pie)
        {
            return IsWriteable(pie.Property);
        }

        /// <summary>
        /// Get the Primary Key of the Model T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="method"></param>
        /// <returns></returns>
        public static PropertyInfoExtended GetSingleKey<T>(string method)
        {
            var type = typeof(T);
            var keys = KeyPropertiesCache(type);
            var explicitKeys = ExplicitKeyPropertiesCache(type);
            var keyCount = keys.Count + explicitKeys.Count;
            if (keyCount > 1)
                throw new Exception($"{method}<T> only supports an entity with a single [Key] or [ExplicitKey] property");
            if (keyCount == 0)
                throw new Exception($"{method}<T> only supports an entity with a [Key] or an [ExplicitKey] property");

            return keys.Count > 0 ? keys[0] : explicitKeys[0];
        }

        #endregion

    }
}
