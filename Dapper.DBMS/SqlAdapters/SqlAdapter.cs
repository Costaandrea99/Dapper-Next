﻿using System;
using System.Text;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// The abstract class for all database operations
    /// </summary>
    public abstract class SqlAdapter
    {

        /// <summary>
        /// Adds the name of a column.
        /// </summary>
        /// <param name="sb">The string builder  to append to.</param>
        /// <param name="columnName">The column name.</param>
        public virtual void AppendColumnName(StringBuilder sb, string columnName)
        {
            throw new Exception("Method not implemented for this database yet!");
        }

        /// <summary>
        /// Adds a column equality to a parameter.
        /// </summary>
        /// <param name="sb">The string builder  to append to.</param>
        /// <param name="columnName">The column name.</param>
        public virtual void AppendColumnNameEqualsValue(StringBuilder sb, string columnName)
        {
            throw new Exception("Method not implemented for this database yet!");
        }

        /// <summary>
        /// Get the correct format of table Name
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public virtual string GetTableNameFormat(string tableName)
        {
            throw new Exception("Method not implemented for this database yet!");
        }

        /// <summary>
        /// Get the correct format of column Name
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public virtual string GetColumnNameFormat(string columnName)
        {
            throw new Exception("Method not implemented for this database yet!");
        }

    }
}
