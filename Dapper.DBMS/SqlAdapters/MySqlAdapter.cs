﻿using System.Text;

namespace Dapper.DBMS.SqlAdapters
{

    /// <summary>
    /// The MySQL database adapter.
    /// </summary>
    public partial class MySqlAdapter : SqlAdapter
    {

        /// <summary>
        /// Get the correct format of table Name
        /// </summary>
        /// <param name="tableName">Original Table Name</param>
        /// <returns></returns>
        public override string GetTableNameFormat(string tableName)
        {
            return tableName;
        }

        /// <summary>
        /// Get the correct format of column Name
        /// </summary>
        /// <param name="columnName">Original Column Name</param>
        /// <returns></returns>
        public override string GetColumnNameFormat(string columnName)
        {
            return string.Format("`{0}`", columnName);
        }

        /// <summary>
        /// Adds the name of a column.
        /// </summary>
        /// <param name="sb">The string builder  to append to.</param>
        /// <param name="columnName">The column name.</param>
        public override void AppendColumnName(StringBuilder sb, string columnName)
        {
            sb.Append(GetColumnNameFormat(columnName));
        }

        /// <summary>
        /// Adds a column equality to a parameter.
        /// </summary>
        /// <param name="sb">The string builder  to append to.</param>
        /// <param name="columnName">The column name.</param>
        public override void AppendColumnNameEqualsValue(StringBuilder sb, string columnName)
        {
            sb.AppendFormat("{0} = @{1}", GetColumnNameFormat(columnName), columnName);
        }

    }
}
